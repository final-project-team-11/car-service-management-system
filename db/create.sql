create table car_management_system.car_brands
(
    car_brand_id int auto_increment
        primary key,
    brand_name   varchar(32) not null
);

create table car_management_system.car_models
(
    car_model_id int auto_increment
        primary key,
    model_name   varchar(32) not null,
    car_brand_id int         not null,
    constraint car_models_car_brands_car_brand_id_fk
        foreign key (car_brand_id) references car_management_system.car_brands (car_brand_id)
);

create table car_management_system.reports
(
    report_id           int auto_increment
        primary key,
    visit_id            int         not null,
    brand               varchar(32) not null,
    model               varchar(32) not null,
    year_of_production  year        not null,
    vin                 varchar(17) not null,
    license_plate       varchar(8)  not null,
    execution_date      datetime    not null,
    finish_date         datetime    not null,
    customer_username   varchar(32) not null,
    customer_first_name varchar(32) not null,
    customer_last_name  varchar(32) not null,
    customer_email      varchar(32) null
);

create table car_management_system.report_services
(
    report_service_id int auto_increment
        primary key,
    report_id         int              null,
    service           varchar(32)      not null,
    price             double default 0 not null,
    constraint report_services_reports_fk
        foreign key (report_id) references car_management_system.reports (report_id)
            on delete cascade
);

create table car_management_system.roles
(
    role_id   int auto_increment
        primary key,
    role_name varchar(20) not null
);

create table car_management_system.services
(
    service_id   int auto_increment
        primary key,
    service_name varchar(30) not null,
    price        double      not null,
    constraint services_pk2
        unique (service_name)
);

create table car_management_system.status
(
    status_id   int auto_increment
        primary key,
    status_name varchar(30) not null
);

create table car_management_system.users
(
    user_id      int auto_increment
        primary key,
    first_name   varchar(32)  not null,
    last_name    varchar(32)  not null,
    email        varchar(255) not null,
    phone_number varchar(10)  not null,
    role_id      int          not null,
    username     varchar(20)  not null,
    password     varchar(255) not null,
    constraint users_pk2
        unique (username),
    constraint users_pk3
        unique (email),
    constraint users_pk4
        unique (phone_number),
    constraint users_roles_role_id_fk
        foreign key (role_id) references car_management_system.roles (role_id)
);

create table car_management_system.cars
(
    car_id             int auto_increment
        primary key,
    VIN                varchar(17) not null,
    licence_plate      varchar(8)  not null,
    year_of_production int(4)      null,
    car_model_id       int         not null,
    user_id            int         not null,
    constraint cars_pk2
        unique (VIN),
    constraint cars_pk3
        unique (licence_plate),
    constraint cars_car_models_car_model_id_fk
        foreign key (car_model_id) references car_management_system.car_models (car_model_id),
    constraint cars_users_user_id_fk
        foreign key (user_id) references car_management_system.users (user_id)
            on delete cascade
);

create table car_management_system.visits
(
    visit_id       int auto_increment
        primary key,
    car_id         int      not null,
    order_date     datetime not null,
    execution_date datetime not null,
    status_id      int      not null,
    constraint visits_cars_car_id_fk
        foreign key (car_id) references car_management_system.cars (car_id)
            on delete cascade,
    constraint visits_status_status_id_fk
        foreign key (status_id) references car_management_system.status (status_id)
);

create table car_management_system.visit_services
(
    visit_service_id int auto_increment
        primary key,
    visit_id         int not null,
    service_id       int not null,
    constraint visit_services_services_service_id_fk
        foreign key (service_id) references car_management_system.services (service_id)
            on delete cascade,
    constraint visit_services_visits_visit_id_fk
        foreign key (visit_id) references car_management_system.visits (visit_id)
            on delete cascade
);

