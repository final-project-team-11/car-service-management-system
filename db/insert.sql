-- Insert car brands
INSERT INTO car_brands (brand_name) VALUES
                                        ('Ford'), ('Chevrolet'), ('Honda'), ('Toyota'), ('Volkswagen'), ('BMW'),
                                        ('Nissan'), ('Mercedes'), ('Audi'), ('Renault');

-- Insert car models
INSERT INTO car_models (model_name, car_brand_id) VALUES
                                                      ('Mustang', 1), ('F-150', 1), ('Camaro', 2), ('Corvette', 2),
                                                      ('Brio', 3), ('City', 3), ('Corolla', 4), ('Yaris', 4), ('Golf', 5),
                                                      ('Arteon', 5), ('Passat', 5), ('iX', 6), ('i4', 6), ('Micra', 7),
                                                      ('Note', 7), ('Benz C', 8), ('Benz S', 8), ('A1', 9), ('Q6', 9),
                                                      ('Clio', 10), ('Megane', 10);

-- Insert roles
INSERT INTO roles (role_name) VALUES
                                  ('Customer'), ('Employee');

-- Insert services
INSERT INTO services (service_name, price) VALUES
                                               ('Oil Change', 29.99), ('Tire Rotation', 19.99), ('Brake Inspection', 39.99),
                                               ('Engine Diagnostic', 49.99), ('Transmission Service', 99.99);

-- Insert status
INSERT INTO status (status_name) VALUES
                                     ('Scheduled'), ('In Progress'), ('Completed');

-- Insert users
INSERT INTO users (first_name , last_name,username, password, email, phone_number, role_id) VALUES
                                                                         ('Petar','Petrov','admin', 'password', 'admin@example.com', '1234567890', 2),
                                                                         ('Ivan','Genadiev','mechanic1', 'password', 'mechanic1@example.com', '2345678901', 2),
                                                                         ('Gospodin','Filipov','mechanic2', 'password', 'mechanic2@example.com', '3456789012', 2),
                                                                         ('Hannah','Montana','customer1', 'password', 'customer1@example.com', '4567890123', 1),
                                                                         ('Spas','Dupchiev','customer2', 'password', 'customer2@example.com', '5678901234', 1),
                                                                         ('Dido','Didov','customer3', 'password', 'customer3@example.com', '1548796325', 1),
                                                                         ('Mihail','Iliev','customer4', 'password', 'customer4@example.com', '1234569870', 1),
                                                                         ('Stanislav','Yordanov','customer5', 'password', 'customer5@example.com', '6547893258', 1),
                                                                         ('Margarita','Mihneva','customer6', 'password', 'customer6@example.com', '2225871169', 1),
                                                                         ('Johny','Jonev','customer7', '@johnnyBrav0', 'johnnybravoo212223@gmail.com', '1025871169', 1),
                                                                         ('Petar','Petrov','employee2', '@petersonD@vid', 'petersondavidomein@gmail.com', '3325871169', 2);

-- Insert cars
INSERT INTO cars (VIN, licence_plate, year_of_production, car_model_id, user_id) VALUES
                                                                                     ('1FTMF1EF5DFD01685', 'DEF456', 2013, 2, 5),
                                                                                     ('1G1YY32G035120606', 'JK0123A', 2005, 4, 5),
                                                                                     ('12345678901234166', 'AB1224VG', 2016, 3, 6),
                                                                                     ('12345478901234566', 'B1034VM', 2016, 1, 6),
                                                                                     ('1FTMF1EF5AAD01685', 'A3333AB', 2014, 5, 7),
                                                                                     ('1FTMF1EF5DFD01644', 'BN3443MN', 2009, 7, 8),
                                                                                     ('1ANIF1EF5DFD01685', 'M5556AA', 2020, 9, 8),
                                                                                     ('1ADAD1EF11FD01681', 'R3321VN', 2020, 11, 8),
                                                                                     ('1AAAA1EF5DFD01335', 'V0904AM', 2014, 13, 9),
                                                                                     ('1NMMA1EF5DFD01611', 'E5688N', 2010, 15, 9),
                                                                                     ('1TAMF1EF5DFD01666', 'S4876MN', 2010, 17, 9);

-- Insert visits
INSERT INTO visits (car_id, order_date, execution_date, status_id) VALUES
                                                                       (1, '2023-04-09 10:00:00', '2023-04-09 11:00:00', 1),
                                                                       (2, '2023-04-09 12:00:00', '2023-04-09 14:00:00', 1),
                                                                       (3, '2023-04-09 16:00:00', '2023-04-09 18:00:00', 1),
                                                                       (4, '2023-04-10 10:00:00', '2023-04-10 12:00:00', 1);

-- Insert visit services
INSERT INTO visit_services (visit_id, service_id) VALUES
                                                      (1, 1), (1, 2), (1, 3),
                                                      (2, 1), (2, 4), (2, 5),
                                                      (3, 2), (3, 4),
                                                      (4, 1), (4, 3), (4, 5);
