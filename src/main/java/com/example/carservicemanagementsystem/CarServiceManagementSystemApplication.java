package com.example.carservicemanagementsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarServiceManagementSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarServiceManagementSystemApplication.class, args);

        System.out.println("\u001B[44m" + "\u001B[30m" + "   Team 11   " + "\u001B[0m");

    }
}
