package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.DuplicateEntityException;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.exceptions.UnauthorizedException;
import com.example.carservicemanagementsystem.helpers.email.EmailSenderService;
import com.example.carservicemanagementsystem.helpers.user.UserModifyDto;
import com.example.carservicemanagementsystem.helpers.user.UserPasswordChangeDto;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.models.Visit;
import com.example.carservicemanagementsystem.repositories.UserRepository;
import com.example.carservicemanagementsystem.services.contracts.RoleService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import com.example.carservicemanagementsystem.services.contracts.VisitService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;


import java.time.LocalDate;
import java.util.*;


@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final EmailSenderService emailSenderService;
    private final UserRepository userRepository;
    private final RoleService roleService;
    private VisitService visitService;

    public VisitService visitService() {
        return visitService;
    }

    @Autowired
    public void setVisitService(@Lazy VisitService visitService) {
        this.visitService = visitService;
    }

    @Override
    public User getById(Integer id) {
        User user = userRepository.getByUserId(id);
        if (user == null) {
            throw new NotFoundException(String.format("User with id %d not found.", id));
        }
        return user;
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.getByUsername(username);
        if (user == null) {
            throw new NotFoundException(String.format("User with username %s not found.", username));
        }
        return user;
    }

    @Override
    public User getByEmail(String email) {
        User user = userRepository.getByEmail(email);
        if (user == null) {
            throw new NotFoundException(String.format("User with email %s not found.", email));
        }
        return user;
    }

    @Override
    public User getByPhoneNumber(String phoneNumber) {
        User user = userRepository.getByPhoneNumber(phoneNumber);
        if (user == null) {
            throw new NotFoundException(String.format("User with phone number %s not found.", phoneNumber));
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        return new ArrayList<>(userRepository.findAll());
    }

    @Override
    public List<User> getByAttributes(Integer userId,
                                      String username,
                                      String phoneNumber,
                                      String email) {
        return userRepository.findUsersByAttributes(userId, username, phoneNumber, email);
    }

    @Override
    public void createUser(User user) {
        emailSenderService.sendCredentials(user);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(User user) {

        userRepository.delete(user);
    }


    @Override
    public void updateUser(User user,
                           Optional<String> firstName,
                           Optional<String> lastName,
                           Optional<String> email,
                           Optional<String> username,
                           Optional<String> phoneNumber,
                           Optional<Integer> roleId) {

        firstName.ifPresent(user::setFirstName);
        lastName.ifPresent(user::setLastName);

        email.ifPresent(value -> {
            changeUserEmail(user, value);
        });

        username.ifPresent(value -> {
            changeUserUsername(user, value);
        });

        phoneNumber.ifPresent(value -> {
            changePhoneNumber(user, value);
        });

        roleId.ifPresent(value -> {
            changeRole(user, value);
        });

        userRepository.save(user);
    }

    @Override
    public void updateUser(User owner, UserModifyDto modifyDto, BindingResult bindingResult) {
        if (modifyDto.getFirst_name() != null && !modifyDto.getFirst_name().isBlank()) {
            owner.setFirstName(modifyDto.getFirst_name());
        }
        if (modifyDto.getLast_name() != null && !modifyDto.getLast_name().isBlank()) {
            owner.setLastName(modifyDto.getLast_name());
        }
        if (modifyDto.getUsername() != null && !modifyDto.getUsername().isBlank()) {
            owner.setUsername(modifyDto.getUsername());
        }
        if (modifyDto.getEmail() != null && !modifyDto.getEmail().isBlank()) {
            owner.setEmail(modifyDto.getEmail());
        }
        if (modifyDto.getPhone_number() != null && !modifyDto.getPhone_number().isBlank()) {
            owner.setPhoneNumber(modifyDto.getPhone_number());
        }
        if (modifyDto.getRole_id() != null && !modifyDto.getRole_id().equals(0)) {
            owner.setRole(roleService.getById(modifyDto.getRole_id()));
        }
        userRepository.save(owner);
    }

    private void changeUserEmail(User user, String email) {
        if (user.getEmail().equals(email)) {
            return;
        }
        try {
            getByEmail(email);
        } catch (NotFoundException e) {
            user.setEmail(email);
            return;
        }
        throw new DuplicateEntityException("User", "email", email);
    }

    private void changeUserUsername(User user, String username) {
        if (user.getUsername().equals(username)) {
            return;
        }
        try {
            getByUsername(username);
        } catch (NotFoundException e) {
            user.setUsername(username);
            return;
        }
        throw new DuplicateEntityException("User", "username", username);
    }

    private void changePhoneNumber(User user, String phoneNumber) {
        if (user.getPhoneNumber().equals(phoneNumber)) {
            return;
        }
        try {
            getByPhoneNumber(phoneNumber);
        } catch (NotFoundException e) {
            user.setPhoneNumber(phoneNumber);
            return;
        }
        throw new DuplicateEntityException("User", "phone number", phoneNumber);
    }

    private void changeRole(User user, Integer roleId) {
        user.setRole(roleService.getById(roleId));
    }

    @Override
    public void changeUserPassword(User user, UserPasswordChangeDto passDto) {

        if (!checkOldPassword(user.getPassword(), passDto.getOld_password())) {
            throw new IllegalArgumentException("The old password is incorrect.");
        }

        if (user.getPassword().equalsIgnoreCase(passDto.getNew_password())) {
            throw new IllegalArgumentException("Your current password cannot be your new password.");
        }

        if (!checkPasswordAndConfirmPassword(passDto.getNew_password(), passDto.getConfirmNewPassword())) {
            throw new IllegalArgumentException("Confirm password doesn't match the new password");
        }
        user.setPassword(passDto.getNew_password());
        userRepository.save(user);
    }

    private boolean checkPasswordAndConfirmPassword(String newPassword, String confirmNewPassword) {
        return newPassword.equals(confirmNewPassword);
    }

    private boolean checkOldPassword(String password, String oldPassword) {
        return password.equals(oldPassword);
    }

    public void checkIfUserIsAuthorized(User user) {
        if (user.getRole().getId() == 1) {
            throw new UnauthorizedException(String.format("%s is not authorized to perform this action.",
                    user.getUsername()));
        }
    }


    //MVC
    @Override
    public List<User> getFromInput(String query,
                                   Integer brandId,
                                   String order,
                                   LocalDate dateOne,
                                   LocalDate dateTwo) {

        Set<User> users = new HashSet<>();
        addByVisitsBetweenDates(dateOne, dateTwo, users);
        addByBrandId(brandId, users);
        addBySearchInput(query, users);

        List<User> userList = new ArrayList<>(users);
        orderUsersByChoice(order, userList);
        return userList;
    }

    private void orderUsersByChoice(String order, List<User> userList) {
        switch (order.toLowerCase()) {
            case "asc":
                userList.sort(Comparator.comparing(User::getFirstName));
                break;
            case "desc":
                userList.sort(Comparator.comparing(User::getFirstName).reversed());
                break;
            default:
                userList.sort(Comparator.comparing(User::getId));
                break;
        }
    }

    private void addByVisitsBetweenDates(LocalDate dateOne, LocalDate dateTwo, Set<User> users) {
        List<Visit> visits = visitService().getAll();
        if (dateOne == null && dateTwo == null) {
            return;
        }
        if (dateTwo == null) {
            users.addAll(visits.stream().filter(v -> v.getExecutionDate().toLocalDate().isAfter(dateOne))
                    .map(Visit::getCar)
                    .map(Car::getUser).toList());
            return;
        }
        if (dateOne == null) {
            users.addAll(visits.stream().filter(v -> v.getExecutionDate().toLocalDate().isBefore(dateTwo))
                    .map(Visit::getCar)
                    .map(Car::getUser).toList());
            return;
        }

        users.addAll(visits.stream().filter(v -> v.getExecutionDate().toLocalDate().isAfter(dateOne) &&
                        v.getExecutionDate().toLocalDate().isBefore(dateTwo))
                .map(Visit::getCar)
                .map(Car::getUser).toList());
    }

    private void addByBrandId(Integer brandId, Set<User> users) {
        List<User> usersList = new ArrayList<>(getAll());
        for (User user : usersList) {
            if (user.getCars()
                    .stream().anyMatch(u -> u.getCarModel()
                            .getCarBrand()
                            .getId().equals(brandId))) {
                users.add(user);
            }
        }
    }

    private void addBySearchInput(String query, Set<User> users) {
        List<String> keywords = Arrays.stream(query.split(" ")).toList();
        for (String keyword : keywords) {
            if (isInteger(keyword)) {
                User user = userRepository.getByUserId(Integer.parseInt(keyword));
                if (user != null) {
                    users.add(user);
                }
                user = userRepository.getByPhoneNumber(keyword);
                if (user != null) {
                    users.add(user);
                }
            } else {
                users.addAll(userRepository.findByKeyword(keyword));
            }
        }
    }


    public boolean isInteger(String strNum) {
        try {
            Integer.parseInt(strNum);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}