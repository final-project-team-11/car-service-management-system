package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.DuplicateEntityException;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.models.Status;
import com.example.carservicemanagementsystem.repositories.StatusRepository;
import com.example.carservicemanagementsystem.services.contracts.StatusService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @Override
    public List<Status> getAll() {
        List<Status> statuses = statusRepository.findAll();
        if (statuses.isEmpty()) {
            throw new NotFoundException("There aren't status yet");
        }

        return statuses;
    }

    @Override
    public Status getById(Integer statusId) {
        return statusRepository.findById(statusId)
                .orElseThrow(() -> new EntityNotFoundException("Status with id " + statusId + " not found"));

    }

    public List<Status> getByStatusContain(Integer statusId, String statusName) {
        List<Status> statuses = statusRepository.getByStatusContain(statusId, statusName);

        if (statusId != null) {
            getById(statusId);
        }

        if (statuses.isEmpty()) {
            throw new NotFoundException("There aren't status with this parameter(s)");
        }
        return statuses;
    }

    @Override
    public Status create(Status status) {
        if (getAll()
                .stream()
                .anyMatch(existingStatus -> existingStatus.getStatusName().equals(status.getStatusName()))) {
            throw new DuplicateEntityException("Status", "name", status.getStatusName());
        }

        return statusRepository.save(status);
    }

    @Override
    public void deleteById(Integer statusId) {
        // TODO: 19.04.2023 г. ако не е null го взимаме обаче ако е null си го направил да се изтрива?
        if (statusId != null) {
            getById(statusId);
        }
        statusRepository.deleteById(statusId);
    }
}
