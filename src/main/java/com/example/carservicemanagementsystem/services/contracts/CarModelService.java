package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.helpers.car.CreateCarModelDto;
import com.example.carservicemanagementsystem.models.CarModel;

public interface CarModelService {

    CarModel getById(Integer id);

    CarModel create(CreateCarModelDto model);

    CarModel getModelByName(String modelName);
}
