package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.models.Service;

import java.util.List;

public interface ServiceService {
    List<Service> getAll();

    Service getById(Integer serviceId);

    List<Service> getByServiceContain(Integer id, String serviceName, Double priceBefore, Double priceAfter);

    Service create(Service service);

    Service update(Integer id, Service service);

    void deleteById(Integer serviceId);
}
