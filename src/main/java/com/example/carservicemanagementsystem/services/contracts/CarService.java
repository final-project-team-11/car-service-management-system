package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.helpers.car.CreateCarDto;
import com.example.carservicemanagementsystem.helpers.car.UpdateCarDto;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.User;

import java.util.List;

public interface CarService {

    List<Car> getAll();

    Car getById(Integer id);

    Car getByLicencePlate(String licencePlate);

    Car getByVin(String vin);

    List<Car> getByPhoneNumber(String phoneNumber);

    List<Car> getCars(String model, String brand, Integer yearOfProduction);

    Car update(UpdateCarDto car, Integer id);

    Car create(CreateCarDto car, User user);

    List<Car> findByKeyword(String keyword);
}
