package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.helpers.user.UserModifyDto;
import com.example.carservicemanagementsystem.helpers.user.UserPasswordChangeDto;
import com.example.carservicemanagementsystem.models.User;
import org.springframework.validation.BindingResult;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface UserService {
    User getById(Integer id);

    User getByUsername(String username);

    User getByEmail(String email);

    User getByPhoneNumber(String phoneNumber);

    List<User> getAll();

    List<User> getByAttributes(Integer userId,
                               String username,
                               String phoneNumber,
                               String email);

    void createUser(User user);

    void deleteUser(User user);

    void updateUser(User user,
                    Optional<String> firstName,
                    Optional<String> lastName,
                    Optional<String> email,
                    Optional<String> username,
                    Optional<String> phoneNumber,
                    Optional<Integer> roleId);

    void updateUser(User owner, UserModifyDto modifyDto, BindingResult bindingResult);

    void changeUserPassword(User user, UserPasswordChangeDto passDto);

    void checkIfUserIsAuthorized(User user);

    List<User> getFromInput(String query, Integer brandId, String order, LocalDate dateOne, LocalDate dateTwo);
}
