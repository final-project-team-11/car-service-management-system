package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.models.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(Integer statusId);

    List<Status> getByStatusContain(Integer statusId, String statusName);

    Status create(Status status);

    void deleteById(Integer statusId);

}
