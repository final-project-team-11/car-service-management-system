package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.models.Role;

import java.util.List;

public interface RoleService {

    Role getById(Integer id);

    Role getByName(String name);

    List<Role> getAll();
}
