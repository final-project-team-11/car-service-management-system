package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.helpers.car.CreateCarBrandDto;
import com.example.carservicemanagementsystem.models.CarBrand;

import java.util.List;

public interface CarBrandService {

    CarBrand getById(Integer id);

    CarBrand create(CreateCarBrandDto brand);

    List<CarBrand> getAll();
}
