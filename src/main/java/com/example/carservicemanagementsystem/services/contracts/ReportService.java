package com.example.carservicemanagementsystem.services.contracts;


import com.example.carservicemanagementsystem.helpers.report.ReportDto;
import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.Visit;

import java.time.LocalDateTime;
import java.util.List;

public interface ReportService {

    Report getById(Integer id);

    List<Report> getAll();

    List<ReportDto> getReportDtosByParameters(Integer reportId, String licensePlate, LocalDateTime executionDate, LocalDateTime finishDate);

    List<ReportDto> getByDatetime(String date, LocalDateTime firstDate, LocalDateTime secondDate);

    void create(Visit visit);

    void sendReportToCustomer(Integer reportId, String currency);
}
