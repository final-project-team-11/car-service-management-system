package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.helpers.visits.EditVisitDto;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.models.Visit;

import java.time.LocalDateTime;
import java.util.List;

public interface VisitService {

    List<Visit> getAll();

    Visit getById(Integer visitId);

    List<Visit> getByStatus(Integer statusId, String statusName);

    List<Visit> getByCar(Integer id, String licence, String vin, Integer year);

    List<Visit> getByDate(String dateType, LocalDateTime start, LocalDateTime end,
                          LocalDateTime dateBefore, LocalDateTime dateAfter);

    List<Visit> getByServices(Integer serviceId);

    Visit create(Visit visit, String carType, String carValue);

    Visit addServiceToVisit(Integer visitId, Integer serviceId);

    void deleteServiceFromVisit(Integer visitId, Integer serviceId);

    Visit update(EditVisitDto editVisitDto, Integer id);

    void deleteById(Integer visitId);

    void deleteByCar(Integer id, String licence, String vin, Integer year);

    void deleteByStatus(Integer statusId, String statusName);

    void deleteByDate(String dateType, LocalDateTime start, LocalDateTime end,
                      LocalDateTime dateBefore, LocalDateTime dateAfter);

    int getUserVisits(User owner);

    List<Visit> findByKeyword(String keyword);
}