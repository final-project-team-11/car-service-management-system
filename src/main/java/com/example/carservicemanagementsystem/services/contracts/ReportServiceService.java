package com.example.carservicemanagementsystem.services.contracts;

import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.ReportService;
import com.example.carservicemanagementsystem.models.Visit;

import java.util.List;

public interface ReportServiceService {

    int numberOfServices(Integer reportId);

    double totalPriceOfServices(Report report);

    void addServices(Report report, Visit visit);

    List<ReportService> getReportServices(Report report);
}