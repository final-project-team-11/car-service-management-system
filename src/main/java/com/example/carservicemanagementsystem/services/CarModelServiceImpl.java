package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.helpers.car.CreateCarModelDto;
import com.example.carservicemanagementsystem.models.CarModel;
import com.example.carservicemanagementsystem.repositories.CarModelRepository;
import com.example.carservicemanagementsystem.services.contracts.CarBrandService;
import com.example.carservicemanagementsystem.services.contracts.CarModelService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarModelServiceImpl implements CarModelService {

    private final CarModelRepository modelRepository;
    private final ModelMapper modelMapper;
    private final CarBrandService brandService;

    @Override
    public CarModel getById(Integer id) {
        return modelRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Model with id: " + id + " not found."));
    }

    @Override
    public CarModel create(CreateCarModelDto model) {

        CarModel newModel = modelMapper.map(model, CarModel.class);
        newModel.setCarBrand(brandService.getById(model.getCarBrandId()));
        return modelRepository.save(newModel);
    }

    @Override
    public CarModel getModelByName(String modelName) {
        CarModel model = modelRepository.findCarModelByModelName(modelName);
        if (model == null) {
            throw new NotFoundException("Model not found");
        }
        return model;
    }
}