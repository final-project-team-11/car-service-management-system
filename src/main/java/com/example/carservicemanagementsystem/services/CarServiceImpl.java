package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.BadRequestException;
import com.example.carservicemanagementsystem.exceptions.EntityNotFoundException;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.helpers.car.CreateCarDto;
import com.example.carservicemanagementsystem.helpers.car.UpdateCarDto;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.repositories.CarRepository;
import com.example.carservicemanagementsystem.services.contracts.CarModelService;
import com.example.carservicemanagementsystem.services.contracts.CarService;
import com.example.carservicemanagementsystem.services.contracts.UserService;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final ModelMapper modelMapper;
    private final CarModelService carModelService;
    private final UserService userService;

    private static final String BULGARIAN_LICENSE_PLATE_PATTERN = "^[A-Z]{1,2}\\d{4}[A-Z]{1,2}$";

    @Override
    public List<Car> getAll() {
        return carRepository.findAll();
    }

    @Override
    public Car getById(Integer id) {
        return carRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Car with id " + id + " not found"));
    }

    @Override
    public Car getByLicencePlate(String licencePlate) {
        Car car = carRepository.getByLicencePlate(licencePlate);
        if (car == null) {
            throw new NotFoundException("Car not found.");
        }
        return car;
    }

    @Override
    public Car getByVin(String vin) {
        Car car = carRepository.getByVin(vin);
        if (car == null) {
            throw new NotFoundException("Car not found");
        }
        return car;
    }

    @Override
    public List<Car> getByPhoneNumber(String phoneNumber) {
        List<Car> cars = carRepository.getByPhoneNumber(phoneNumber);
        if (cars.isEmpty()) {
            throw new NotFoundException("This user doesn't have cars.");
        }
        return cars;
    }

    @Override
    public List<Car> getCars(String model, String brand, Integer yearOfProduction) {
        return carRepository.findCars(model, brand, yearOfProduction);
    }

    @Override
    public Car update(UpdateCarDto car, Integer id) {
        Car updatedCar = carRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User with id: " + id + " does not exist"));
        updatedCar.setVin(car.getVin());
        updatedCar.setLicencePlate(car.getLicencePlate());
        updatedCar.setYear(car.getYear());
        updatedCar = carRepository.save(updatedCar);
        return modelMapper.map(updatedCar, Car.class);
    }

    @Override
    public Car create(CreateCarDto car, User user) {
        Car newCar = modelMapper.map(car, Car.class);
        if (isBulgarianLicensePlate(newCar.getLicencePlate())) {
            newCar.setUser(user);
            newCar.setCarModel(carModelService.getModelByName(car.getModel()));
            return carRepository.save(newCar);
        } else {
            throw new BadRequestException("Licence plate: " + newCar.getLicencePlate() + " is not valid.");
        }
    }

    @Override
    public List<Car> findByKeyword(String keyword) {
        if (keyword == null || keyword.isBlank()) {
            return carRepository.findAll();
        }
        return carRepository.findByKeyword(keyword);
    }

    public boolean isBulgarianLicensePlate(String licensePlate) {
        Pattern pattern = Pattern.compile(BULGARIAN_LICENSE_PLATE_PATTERN);
        Matcher matcher = pattern.matcher(licensePlate);
        return matcher.matches();
    }
}