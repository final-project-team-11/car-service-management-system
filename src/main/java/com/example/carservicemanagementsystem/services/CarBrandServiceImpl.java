package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.helpers.car.CreateCarBrandDto;
import com.example.carservicemanagementsystem.models.CarBrand;
import com.example.carservicemanagementsystem.repositories.CarBrandRepository;
import com.example.carservicemanagementsystem.services.contracts.CarBrandService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CarBrandServiceImpl implements CarBrandService {

    private final CarBrandRepository brandRepository;
    private final ModelMapper modelMapper;

    @Override
    public CarBrand getById(Integer id) {
        return brandRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Brand with id: " + id + " not found."));
    }

    @Override
    public CarBrand create(CreateCarBrandDto brand) {

        CarBrand newBrand = modelMapper.map(brand, CarBrand.class);
        return brandRepository.save(newBrand);
    }

    @Override
    public List<CarBrand> getAll() {
        return brandRepository.findAll();
    }
}