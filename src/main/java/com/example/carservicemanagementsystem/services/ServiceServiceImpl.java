package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.DuplicateEntityException;
import com.example.carservicemanagementsystem.exceptions.EntityNotFoundException;
import com.example.carservicemanagementsystem.exceptions.ForbiddenException;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.models.Service;
import com.example.carservicemanagementsystem.repositories.ServiceRepository;
import com.example.carservicemanagementsystem.services.contracts.ServiceService;
import lombok.RequiredArgsConstructor;

import java.util.List;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class ServiceServiceImpl implements ServiceService {
    private final ServiceRepository serviceRepository;

    @Override
    public List<Service> getAll() {
        List<Service> services = serviceRepository.findAll();
        if (services.isEmpty()) {
            throw new NotFoundException("There aren't services yet");
        }

        return services;
    }

    @Override
    public Service getById(Integer serviceId) {
        return serviceRepository.findById(serviceId)
                .orElseThrow(() -> new EntityNotFoundException("Service with id " + serviceId + " not found"));
    }

    @Override
    public List<Service> getByServiceContain(Integer id, String serviceName, Double priceBefore, Double priceAfter) {
        List<Service> services = serviceRepository.getByServiceContain(id, serviceName, priceBefore, priceAfter);

        if (id != null) {
            getById(id);
        }

        if (services.isEmpty()) {
            throw new NotFoundException("There aren't services with this parameter(s)");
        }
        return services;
    }

    @Override
    public Service create(Service service) {
        checkForDoubleAndPrice(service);

        return serviceRepository.save(service);
    }

    @Override
    public Service update(Integer id, Service updateService) {
        if (getById(id).getServiceName().equals(updateService.getServiceName())) {
            if (updateService.getPrice() <= 0) {
                throw new ForbiddenException("Please insert valid price!");
            }
        } else {
            checkForDoubleAndPrice(updateService);
        }

        updateService.setServiceId(id);

        return serviceRepository.save(updateService);
    }

    @Override
    public void deleteById(Integer serviceId) {

        if (serviceId != null) {
            getById(serviceId);
        }

        serviceRepository.deleteById(serviceId);
    }

    private void checkForDoubleAndPrice(Service service) {


        if (getAll()
                .stream()
                .anyMatch(existingService -> existingService.getServiceName().equals(service.getServiceName()))) {
            throw new DuplicateEntityException("Service", "name", service.getServiceName());
        }

        if (service.getPrice() <= 0) {
            throw new ForbiddenException("Please insert valid price!");
        }
    }
}
