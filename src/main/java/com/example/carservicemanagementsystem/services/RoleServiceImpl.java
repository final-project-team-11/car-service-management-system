package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.models.Role;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.repositories.RoleRepository;
import com.example.carservicemanagementsystem.services.contracts.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public Role getById(Integer id) {
        Role role = roleRepository.getById(id);
        if (role == null) {
            throw new NotFoundException(String.format("Role with ID %d not found.", id));
        }
        return role;
    }

    @Override
    public Role getByName(String name) {
        Role role = roleRepository.getByName(name);
        if (role == null) {
            throw new NotFoundException(String.format("Role with name %s not found.", name));
        }
        return role;
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.findAll();
    }
}
