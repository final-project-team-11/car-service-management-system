package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.ReportService;
import com.example.carservicemanagementsystem.models.Service;
import com.example.carservicemanagementsystem.models.Visit;
import com.example.carservicemanagementsystem.repositories.ReportServiceRepository;
import com.example.carservicemanagementsystem.services.contracts.ReportServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ReportServiceServiceImpl implements ReportServiceService {

    public final ReportServiceRepository reportServiceRepository;

    @Override
    public int numberOfServices(Integer reportId) {
        return reportServiceRepository.numberOfServices(reportId);
    }

    @Override
    public double totalPriceOfServices(Report report) {
        Double totalPrice = 0.0;
        for (ReportService rs :
                report.getReportServices()) {
            totalPrice += rs.getPrice();
        }
        return totalPrice;
    }

    @Override
    public void addServices(Report report, Visit visit) {
        List<ReportService> reportServices = new ArrayList<>();
        for (Service service : visit.getServices()) {
            ReportService reportService = new ReportService();
            reportService.setReport(report);
            reportService.setService(service.getServiceName());
            reportService.setPrice(service.getPrice());
            reportServices.add(reportService);
            reportServiceRepository.save(reportService);
        }
        report.setReportServices(reportServices);
    }

    @Override
    public List<ReportService> getReportServices(Report report) {
        System.out.println(reportServiceRepository.getByReportId(report.getReportId()));
        return reportServiceRepository.getByReportId(report.getReportId());
    }
}