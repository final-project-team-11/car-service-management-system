package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.helpers.pdf.PdfCreator;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.helpers.report.ReportDto;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.models.Visit;
import com.example.carservicemanagementsystem.helpers.email.EmailSenderServiceImpl;
import com.example.carservicemanagementsystem.repositories.ReportRepository;
import com.example.carservicemanagementsystem.services.contracts.*;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.FileSystemResource;


import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class ReportServiceImpl implements ReportService {

    public static final String INVALID_CURRENCY = "Please enter a valid currency that we work with.";
    public static final String BLANK_CURRENCY = "Please enter a valid currency.";
    private final ReportRepository reportRepository;
    private final ReportServiceService reportServiceService;
    private final ModelMapper modelMapper;
    private final EmailSenderServiceImpl emailSender;
    private final PdfCreator pdfCreator;

    @Override
    public Report getById(Integer id) {
        Report report = reportRepository.getByReportId(id);
        if (report == null) {
            throw new NotFoundException(String.format("Report with ID %d wasn't found.", id));
        }
        return report;
    }

    @Override
    public List<Report> getAll() {
        return new ArrayList<>(reportRepository.findAll());
    }

    @Override
    public List<ReportDto> getReportDtosByParameters(Integer reportId,
                                                     String licensePlate,
                                                     LocalDateTime executionDate,
                                                     LocalDateTime finishDate) {
        List<Report> reports = reportRepository.findByParameters(
                reportId,
                licensePlate,
                executionDate,
                finishDate);

        return mapReportsToReportDtos(reports);
    }

    @Override
    public void create(Visit visit) {
        Report report = mapVisitIntoReport(visit);
        report = reportRepository.save(report);
        reportServiceService.addServices(report, visit);
        pdfCreator.createPdfWithReportInfo(report, Currency.getInstance("BGN"));
    }


    @Override
    public List<ReportDto> getByDatetime(String date, LocalDateTime after, LocalDateTime before) {
        return switch (date.toLowerCase()) {
            case "execution" -> mapReportsToReportDtos(reportRepository.findReportsByExecutionDate(after, before));
            case "finish" -> mapReportsToReportDtos(reportRepository.findReportsByFinishDate(after, before));
            default ->
                    throw new IllegalArgumentException(String.format("Getting reports by %s is not an option.", date));
        };
    }

    @Override
    public void sendReportToCustomer(Integer reportId, String currencyString) {
        Report report = getById(reportId);

        Currency currency = getCurrencyFromString(currencyString);

        pdfCreator.createPdfWithReportInfo(report, currency);

        FileSystemResource file = findReportPdf(report, currency);

        emailSender.sendPdf(report, file);
    }

    private static Currency getCurrencyFromString(String currencyString) {
        if (currencyString == null || currencyString.isBlank()) {
            throw new IllegalArgumentException(BLANK_CURRENCY);
        }
        Currency currency;
        switch (currencyString.toUpperCase()) {
            case "USD":
            case "EUR":
            case "GBP":
            case "BGN":
                currency = Currency.getInstance(currencyString.toUpperCase());
                break;
            default:
                throw new IllegalArgumentException(INVALID_CURRENCY);
        }
        return currency;
    }

    private static FileSystemResource findReportPdf(Report report, Currency currency) throws NullPointerException {
        return new FileSystemResource(new File(String.format("reports/%s-%s-%s-%s.pdf",
                report.getReportId(),
                report.getFirstName(),
                report.getLastName(),
                currency.getCurrencyCode())));
    }


    private static Report mapVisitIntoReport(Visit visit) {
        Car car = visit.getCar();
        User owner = car.getUser();
        Report report = new Report();
        report.setVisitId(visit.getVisitId());
        report.setBrand(car.getCarModel().getCarBrand().getBrandName());
        report.setModel(car.getCarModel().getModelName());
        report.setYearOfProduction(car.getYear());
        report.setVin(car.getVin());
        report.setLicensePlate(car.getLicencePlate());
        report.setExecutionDate(visit.getExecutionDate());
        report.setFinishDate(LocalDateTime.now());
        report.setUsername(owner.getUsername());
        report.setFirstName(owner.getFirstName());
        report.setLastName(owner.getLastName());
        report.setEmail(owner.getEmail());
        return report;
    }

    public List<ReportDto> mapReportsToReportDtos(List<Report> reports) {
        List<ReportDto> reportDtos = new ArrayList<>();
        for (Report report : reports) {
            ReportDto reportDto = new ReportDto();

            modelMapper.map(report, reportDto);
            reportDto.setServicesNumber(reportServiceService.numberOfServices(report.getReportId()));
            reportDto.setTotalPrice(reportServiceService.totalPriceOfServices(report));
            reportDtos.add(reportDto);
        }
        return reportDtos;
    }
}
