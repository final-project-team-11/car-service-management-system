package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.*;
import com.example.carservicemanagementsystem.helpers.enums.CarIdentifierType;
import com.example.carservicemanagementsystem.helpers.visits.EditVisitDto;
import com.example.carservicemanagementsystem.models.*;
import com.example.carservicemanagementsystem.repositories.VisitRepository;
import com.example.carservicemanagementsystem.services.contracts.*;
import com.example.carservicemanagementsystem.services.contracts.ReportService;
import com.example.carservicemanagementsystem.services.contracts.VisitService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
@RequiredArgsConstructor
public class VisitServiceImpl implements VisitService {
    public static final int STATUS_ID_DEFAULT = 1; //<-- Default status 1 - "Scheduled"
    public static final int COMPLETED = 3;
    private final VisitRepository visitRepository;
    private final StatusService statusService;
    private final ServiceService serviceService;
    private final CarService carService;
    private final ReportService reportService;
    private final ModelMapper modelMapper;

    @Override
    public List<Visit> getAll() {
        List<Visit> visits = visitRepository.findAll();
        if (visits.isEmpty()) {
            throw new NotFoundException("There aren't visits yet");
        }

        return visits;
    }

    @Override
    public Visit getById(Integer visitId) {
        return visitRepository.findById(visitId)
                .orElseThrow(() -> new EntityNotFoundException("Visit with id " + visitId + " not found"));
    }

    @Override
    public List<Visit> getByStatus(Integer statusId, String statusName) {
        List<Visit> visits = visitRepository.getByStatus(statusId, statusName);
        //check if this status exist - only with id, other is a contains values
        if (statusId != null) {
            statusService.getById(statusId);
        }
        //check if there are visits with this/these status's parameter(s)
        if (visits.isEmpty()) {
            throw new NotFoundException("There aren't visits with this Status");
        }

        return visits;
    }

    @Override
    public List<Visit> getByCar(Integer id, String licence, String vin, Integer year) {
        List<Visit> visits = visitRepository.getByCar(id, licence, vin, year);
        //check if this car exist - only with id, others are contains values
        if (id != null) {
            carService.getById(id);
        }
        //check if there are visits with this/these car's parameter(s)
        if (visits.isEmpty()) {
            throw new NotFoundException("There aren't visits with this Car");
        }

        return visits;
    }

    @Override
    public List<Visit> getByDate(String dateType, LocalDateTime start, LocalDateTime end,
                                 LocalDateTime dateBefore, LocalDateTime dateAfter) {
        List<Visit> visits = visitRepository.getVisitsByDate(dateType, start, end, dateBefore, dateAfter);
        // orderBy dateType asc
        // check if there are visits with this/these date's parameter(s)
        if (visits.isEmpty()) {
            throw new NotFoundException("There aren't visits with this Date");
        }

        return visits;
    }

    @Override
    public List<Visit> getByServices(Integer serviceId) {
        List<Visit> visits = visitRepository.getByService(serviceId);
        //check if this service exist - only with id, other is a contains values
        if (serviceId != null) {
            serviceService.getById(serviceId);
        }
        //check if there are visits with this/these status's parameter(s)
        if (visits.isEmpty()) {
            throw new NotFoundException("There aren't visits with this Service");
        }

        return visits;
    }

    @Override
    public Visit create(Visit newVisit, String carType, String carValue) {

        //check if the execution date is valid (> today)
        if (newVisit.getExecutionDate().isBefore(LocalDateTime.now())) {
            throw new ExecutionDateException("Execution date cannot be in the past");
        }

        newVisit.setOrderDate(LocalDateTime.now());

        //check if the carType is valid - inside the switch check if the car exist with specific parameter
        if (carType == null || carType.isEmpty()) {
            throw new BadRequestException("Invalid Car Identifier Type. Choose between ID, VIN or LICENCE PLATE");
        }
        CarIdentifierType carIdentifierType;
        try {
            carIdentifierType = CarIdentifierType.valueOf(carType);
        } catch (IllegalArgumentException e) {
            throw new BadRequestException("Invalid Car Identifier Type. Choose between ID, VIN or LICENCE PLATE");
        }
        switch (carIdentifierType) {
            case ID -> newVisit.setCar(carService.getById(Integer.valueOf(carValue)));
            case VIN -> newVisit.setCar(carService.getByVin(carValue));
            case LICENCE -> newVisit.setCar(carService.getByLicencePlate(carValue));
        }
        //no check - default status for creation --> new Visit is always Scheduled at the beginning
        Status statusDefault = statusService.getById(STATUS_ID_DEFAULT);
        newVisit.setStatus(statusDefault);
        //check if this visit already exists (same car and execution date)
        Integer car = newVisit.getCar().getId();
        LocalDateTime date = newVisit.getExecutionDate();
        if (checkForDuplicateVisit(car, date) > 0) {
            throw new DuplicateEntityException("Visit", "this car on", date.format(DateTimeFormatter.ISO_DATE));
        }

        return visitRepository.save(newVisit);
    }

    @Override
    public Visit addServiceToVisit(Integer visitId, Integer serviceId) {

        Visit visit = getById(visitId);
        Service service = serviceService.getById(serviceId);
        List<Service> existingServices = visit.getServices();
        // Check if the service is already present in the list
        for (Service existingService : existingServices) {
            if (existingService.getServiceId().equals(serviceId)) {
                throw new DuplicateEntityException("Service", "name", service.getServiceName());
            }
        }
        // Add the service to the list if it doesn't already exist
        existingServices.add(service);
        visit.setServices(existingServices);
        return visitRepository.save(visit);

    }

    @Override
    public void deleteServiceFromVisit(Integer visitId, Integer serviceId) {
        Visit visit = getById(visitId);
        Service service = serviceService.getById(serviceId);

        List<Service> existingServices = visit.getServices();
        boolean isPresent = false;
        for (Service existingService : existingServices) {
            if (existingService.getServiceId().equals(serviceId)) {
                isPresent = true;
                break;
            }
        }

        if (isPresent) {
            existingServices.remove(service);
            visit.setServices(existingServices);
            visitRepository.save(visit);
        } else {
            throw new NotFoundException("There isn't this service in the visit");
        }
    }

    @Override
    public Visit update(EditVisitDto editVisitDto, Integer id) {
        Visit updateVisit = visitRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Visit with id: " + id + " does not exist"));
        updateVisit.setStatus(statusService.getById(editVisitDto.getStatusId()));
        updateVisit.setCar(carService.getById(editVisitDto.getCarId()));
        updateVisit.setExecutionDate(editVisitDto.getExecutionDate());

        List<Service> updatedServices = serviceService.getAll().stream()
                .filter(service -> editVisitDto.getServiceIds().contains(service.getServiceId()))
                .collect(Collectors.toCollection(ArrayList::new));

        updateVisit.setServices(updatedServices);
        updateVisit = visitRepository.save(updateVisit);
        if (updateVisit.getStatus().getStatusId() == 3) {
            reportService.create(updateVisit);
        }
        return modelMapper.map(updateVisit, Visit.class);
    }

    @Override
    public void deleteById(Integer visitId) {
        Visit visit = getById(visitId);
        List<Visit> visits = new ArrayList<>();
        visits.add(visit);
        checkForDelete(visits, "ID: " + visitId);
    }

    @Override
    public void deleteByStatus(Integer statusId, String statusName) {
        List<Visit> visits = getByStatus(statusId, statusName);
        checkForDelete(visits, "Status");
    }

    @Override
    public void deleteByCar(Integer carId, String licence, String vin, Integer year) {
        List<Visit> visits = getByCar(carId, licence, vin, year);
        checkForDelete(visits, "Car");
    }

    @Override
    public void deleteByDate(String dateType, LocalDateTime start, LocalDateTime end,
                             LocalDateTime dataBefore, LocalDateTime dateAfter) {
        List<Visit> visits = getByDate(dateType, start, end, dataBefore, dateAfter);
        checkForDelete(visits, "Date");
    }

    private long checkForDuplicateVisit(Integer carId, LocalDateTime executionDate) {
        return getAll()
                .stream()
                .filter(visit -> visit.getCar().getId().equals(carId))
                .filter(visit -> visit.getExecutionDate().toLocalDate().isEqual(executionDate.toLocalDate()))
                .count();
    }

    private void checkForDelete(List<Visit> visits, String nameBy) {
        //check if there are Entity with these parameters
        if (visits.isEmpty()) {
            throw new EntityNotFoundException("There aren't visits with this " + nameBy);
        } else {
            //check if the execution date is not passed
            if (visits
                    .stream().noneMatch(v -> v.getExecutionDate() != null &&
                            v.getExecutionDate().isAfter(LocalDateTime.now()))) {
                throw new ExecutionDateException("There aren't visits to delete, " +
                        "you can delete only visits with Execution Date in the future!");
            } else {
                //check if the status is not advanced
                if (visits
                        .stream().noneMatch(v -> v.getStatus() != null &&
                                v.getStatus().getStatusId() == STATUS_ID_DEFAULT)) {
                    throw new ForbiddenException("There aren't visits to delete, " +
                            "you can delete only visits with not advanced Status");
                } else {
                    visitRepository.deleteAllInBatch(visits);
                }
            }
        }
    }

    @Override
    public int getUserVisits(User owner) {
        int visits = 0;
        for (Car car : owner.getCars()) {
            if (car.getVisits() == null) {
                continue;
            }
            visits += car.getVisits().size();
        }
        return visits;
    }

    @Override
    public List<Visit> findByKeyword(String keyword) {
        if (keyword == null || keyword.isBlank()) {
            return visitRepository.findAll();
        }
        return visitRepository.findByKeyword(keyword);
    }
}
