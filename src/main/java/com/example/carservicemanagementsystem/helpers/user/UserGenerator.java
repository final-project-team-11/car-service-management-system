package com.example.carservicemanagementsystem.helpers.user;

import com.example.carservicemanagementsystem.exceptions.DuplicateEntityException;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.helpers.password_generator.PasswordGenerator;
import com.example.carservicemanagementsystem.models.Role;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.RoleService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserGenerator {

    private final RoleService roleService;
    private final UserService userService;
    private final PasswordGenerator passwordGenerator;

    public User generateUser(UserCreateDto userCreateDto) {
        checkIfUserWithEmailExist(userCreateDto.getEmail());

        String username = generateUsername(userCreateDto.getFirst_name(),
                userCreateDto.getLast_name());
        username = makeUsernameUnique(username);

        String password = passwordGenerator.generatePassword();
        if (password == null) {
            password = generatePassword();
        }

        Role role = getRole(userCreateDto.getRole_id());

        validatePhoneNumber(userCreateDto.getPhone_number());

        return generateUser(
                userCreateDto.first_name,
                userCreateDto.last_name,
                username,
                password,
                userCreateDto.getEmail(),
                userCreateDto.getPhone_number(),
                role);
    }

    private void validatePhoneNumber(String phoneNumber) {
        try {
            userService.getByPhoneNumber(phoneNumber);
            throw new DuplicateEntityException("User", "phone number", phoneNumber);
        } catch (NotFoundException ignored) {
        }
    }

    private String makeUsernameUnique(String username) {
        Set<String> usernames = userService.getAll()
                .stream().
                map(User::getUsername)
                .collect(Collectors.toSet());

        int number = 0;
        while (usernames.contains(username + number)) {
            number++;
        }
        return username + number;
    }

    private User generateUser(String firstName,
                              String lastName,
                              String username,
                              String password,
                              String email,
                              String phoneNumber,
                              Role role) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.setPhoneNumber(phoneNumber);
        user.setRole(role);

        return user;
    }

    private void checkIfUserWithEmailExist(String email) {
        try {
            userService.getByEmail(email);
        } catch (NotFoundException e) {
            return;
        }
        throw new DuplicateEntityException("User", "email", email);
    }

    private static String generateUsername(String firstName, String lastName) {
        String username = "";
        username = addSymbols(firstName, username);
        username += ".";
        username = addSymbols(lastName, username);
        return username.toLowerCase();
    }

    private static String addSymbols(String firstName, String username) {
        if (firstName.length() >= 3) {
            username += firstName.substring(0, 3);
        } else {
            username += firstName;
            for (int i = 0; i < 3 - firstName.length(); i++) {
                username = username.concat("x");
            }
        }
        return username;
    }

    private String generatePassword() {
        String capitalLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String smallLetters = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String specialCharacters = "!@#$%^&*()_+~`|}{[]\\:;?><,./-=";
        String combinedChars = capitalLetters + smallLetters + numbers + specialCharacters;
        Random random = new Random();
        char[] password = new char[8];
        password[0] = capitalLetters.charAt(random.nextInt(capitalLetters.length()));
        password[1] = smallLetters.charAt(random.nextInt(smallLetters.length()));
        password[2] = numbers.charAt(random.nextInt(numbers.length()));
        password[3] = specialCharacters.charAt(random.nextInt(specialCharacters.length()));
        for (int i = 4; i < 8; i++) {
            password[i] = combinedChars.charAt(random.nextInt(combinedChars.length()));
        }
        return new String(password);
    }

    private Role getRole(int roleId) {
        try {
            return roleService.getById(roleId);
        } catch (NotFoundException e) {
            throw new NotFoundException(e.getMessage());
        }
    }
}
