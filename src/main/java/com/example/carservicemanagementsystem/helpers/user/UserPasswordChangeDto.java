package com.example.carservicemanagementsystem.helpers.user;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPasswordChangeDto {
    @NotBlank(message = "Old password cannot be blank")
    public String old_password;

    @Size(min = 8, message = "New password must be equal or more than 8 characters.")
    public String new_password;

    @NotBlank(message = "Confirm password cannot be empty.")
    public String confirmNewPassword;
}
