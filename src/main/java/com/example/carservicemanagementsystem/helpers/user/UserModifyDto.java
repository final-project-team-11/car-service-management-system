package com.example.carservicemanagementsystem.helpers.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserModifyDto {

    @Size(max = 32, message = "First name must be at least 32 characters long")
    public String first_name;
    @Size(max = 32, message = "Last name must be at least 32 characters long.")
    public String last_name;

    @Email(message = "Please enter a valid email address.")
    public String email;

    @Size(max = 32, message = "Username must be at least 32 characters long.")
    public String username;

    public String phone_number;

    public Integer role_id;
}
