package com.example.carservicemanagementsystem.helpers.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class UserCreateDto {
    @NotBlank(message = "First name cannot be empty.")
    @Size(max = 32)
    public String first_name;
    @NotBlank(message = "Last name cannot be empty.")
    @Size(max = 32)
    public String last_name;
    @NotBlank(message = "Email cannot be empty.")
    @Email
    public String email;
    @NotBlank(message = "Phone number cannot be empty.")
    @Size(min = 10, max = 10, message = "Phone number must be 10 digits long.")
    public String phone_number;
    //@NotNull(message = "Role id cannot be empty.")
    public Integer role_id;
}
