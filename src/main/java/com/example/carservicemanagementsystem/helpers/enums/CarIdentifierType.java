package com.example.carservicemanagementsystem.helpers.enums;

public enum CarIdentifierType {
    ID,
    VIN,
    LICENCE
}
