package com.example.carservicemanagementsystem.helpers.password_generator;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
@RequiredArgsConstructor
public class PasswordGeneratorImpl implements PasswordGenerator {
    public String generatePassword() {
        String password = null;
        try {
            URL url = new URL("https://www.passwordrandom.com/query?command=password");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String response = in.readLine();
            in.close();

            return response;
        } catch (IOException e) {
            return null;
        }
    }

}
