package com.example.carservicemanagementsystem.helpers.password_generator;

public interface PasswordGenerator {
    String generatePassword();
}
