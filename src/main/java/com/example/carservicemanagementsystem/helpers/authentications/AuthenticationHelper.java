package com.example.carservicemanagementsystem.helpers.authentications;

import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.exceptions.UnauthorizedException;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String INVALID_CREDENTIALS = "Invalid credentials.";
    private static final String USER_SESSION_ATTRIBUTE = "currentUser";
    private final UserService userService;


    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new UnauthorizedException("The requested action requires authentication.");
        }
        try {
            String credentials = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            if (credentials == null || credentials.isEmpty() || credentials.isBlank()) {
                throw new UnauthorizedException("Please enter your credentials.");
            }

            String username = getUsername(credentials);
            String password = getPassword(credentials);
            return validateUser(username, password);
        } catch (NotFoundException e) {
            throw new UnauthorizedException(INVALID_CREDENTIALS);
        }
    }

    public User validateUser(String username, String password) {

        try {
            User user = userService.getByUsername(username);
            if (user.getPassword().equals(password)) {
                return user;
            }
        } catch (NotFoundException e) {
            throw new UnauthorizedException(INVALID_CREDENTIALS);
        }
        throw new UnauthorizedException(INVALID_CREDENTIALS);
    }


    private String getUsername(String credentials) {
        String[] inputCredentials = credentials.split(" ");
        if (inputCredentials.length != 2) {
            throw new UnauthorizedException("Invalid number of input parameters.");
        }
        return inputCredentials[0];
    }

    private String getPassword(String credentials) {
        String[] inputCredentials = credentials.split(" ");
        return inputCredentials[1];
    }

    public void verifySession(HttpSession httpSession) {

        if (httpSession.getAttribute(USER_SESSION_ATTRIBUTE) == null) {
            throw new UnauthorizedException("Unauthenticated user");
        }
    }

    public boolean checkIfAuthenticated(HttpSession session) {
        return session.getAttribute("viewer") == null;
    }
}
