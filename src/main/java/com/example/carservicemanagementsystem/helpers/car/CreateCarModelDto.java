package com.example.carservicemanagementsystem.helpers.car;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateCarModelDto {

    private String modelName;
    private Integer carBrandId;
}
