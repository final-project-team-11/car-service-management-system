package com.example.carservicemanagementsystem.helpers.car;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateCarBrandDto {

    private String brandName;
}
