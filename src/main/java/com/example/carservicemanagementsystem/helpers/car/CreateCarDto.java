package com.example.carservicemanagementsystem.helpers.car;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateCarDto {

    @NotBlank(message = "VIN should not be empty!")
    @Size(min = 17, max = 17, message = "VIN should be 17 characters long.")
    private String vin;
    @Pattern(regexp = "^[A-Z]{1,2}\\d{4}[A-Z]{1,2}$", message = "Licence plate is not valid.")
    @NotBlank(message = "Licence plate should not be empty!")
    private String licencePlate;
    @NotNull(message = "Year should not be empty!")
    @Positive(message = "Date of creation should be a positive number!")
    private Integer year;
    @NotNull(message = "Model name should not be empty!")
    private String model;
    private String generalError;
}
