package com.example.carservicemanagementsystem.helpers.car;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateCarDto {

    private String brand;

    private String model;
    @Positive(message = "Date of creation should be a positive number!")
    private Integer year;

    @Size(min = 17, max = 17, message = "VIN should be 17 characters long.")
    private String vin;

    @Pattern(regexp = "^[A-Z]{1,2}\\d{4}[A-Z]{1,2}$", message = "Licence plate is not valid.")
    private String licencePlate;
}