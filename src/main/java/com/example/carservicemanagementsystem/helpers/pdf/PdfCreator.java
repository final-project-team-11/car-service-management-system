package com.example.carservicemanagementsystem.helpers.pdf;

import com.example.carservicemanagementsystem.models.Report;

import java.util.Currency;

public interface PdfCreator {
    void createPdfWithReportInfo(Report report, Currency currency);
}
