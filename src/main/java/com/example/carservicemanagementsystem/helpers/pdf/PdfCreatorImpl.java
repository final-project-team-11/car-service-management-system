package com.example.carservicemanagementsystem.helpers.pdf;

import com.example.carservicemanagementsystem.exceptions.PdfCreationException;
import com.example.carservicemanagementsystem.helpers.currency.CurrencyConverter;
import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.ReportService;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Locale;

import static com.itextpdf.text.FontFactory.HELVETICA_BOLD;

@Service
@RequiredArgsConstructor
public class PdfCreatorImpl implements PdfCreator {
    private final CurrencyConverter converter;
    public static final String CREATION_PDF_ERROR_MESSAGE = "There was a problem generating the PDF file.";


    public void createPdfWithReportInfo(Report report, Currency currency) {
        try {
            double currentRate = converter.getRate(currency);
            double sum = 0.0;

            Document document = new Document(PageSize.A4, 50, 50, 50, 50);
            PdfWriter.getInstance(document, new FileOutputStream(
                    String.format("reports/%s-%s-%s-%s.pdf",
                            report.getReportId(),
                            report.getFirstName(),
                            report.getLastName(),
                            currency.getCurrencyCode())));
            document.open();
            Paragraph title = new Paragraph("ASG Car Service System", FontFactory.getFont(HELVETICA_BOLD));
            title.setAlignment(Element.ALIGN_CENTER);
            document.add(title);
            addDocumentIntroduction(report, document);
            addDetailsAboutTheReport(report, document);
            addDetailsAboutTheUser(report, document);
            addDetailsAboutTheCar(report, document);
            addServices(report, currency, currentRate, sum, document);
            document.close();
        } catch (DocumentException | FileNotFoundException e) {
            throw new PdfCreationException(CREATION_PDF_ERROR_MESSAGE);
        }

    }

    private static void addDocumentIntroduction(Report report, Document document) throws DocumentException {
        document.add(new Paragraph("\n"));
        String intro = String.format("Hello, %s %s%n", report.getFirstName(), report.getLastName()) +
                "In this document you will find information about the visit of the car to the server. " +
                "When the session itself started and when it ended. Information about the owner of the car " +
                "and information about the car itself is also provided.";
        document.add(new Paragraph(intro));
        document.add(new Paragraph("\n"));
    }


    private void addDetailsAboutTheReport(Report report, Document document) throws DocumentException {
        document.add(new Paragraph("Report Details:", FontFactory.getFont(HELVETICA_BOLD)));
        document.add(new Paragraph(String.format("Report ID: %d", report.getReportId())));
        document.add(new Paragraph(String.format("Generated from visit with ID: %d", report.getVisitId())));
        document.add(new Paragraph(String.format("Visit execution date: %s", formatDate(report.getExecutionDate()))));
        document.add(new Paragraph(String.format("Visit finish date: %s", formatDate(report.getFinishDate()))));
        document.add(new Paragraph("\n"));
    }

    private static void addDetailsAboutTheUser(Report report, Document document) throws DocumentException {
        document.add(new Paragraph("User Details:", FontFactory.getFont(HELVETICA_BOLD)));
        document.add(new Paragraph(String.format("Names: %s %s", report.getFirstName(), report.getLastName())));
        document.add(new Paragraph(String.format("Username: %s", report.getUsername())));
        document.add(new Paragraph(String.format("Email: %s", report.getEmail())));
        document.add(new Paragraph("\n"));
    }

    private static void addDetailsAboutTheCar(Report report, Document document) throws DocumentException {
        document.add(new Paragraph("Car Details:", FontFactory.getFont(HELVETICA_BOLD)));
        document.add(new Paragraph(String.format("Brand & Model: %s %s;", report.getBrand(), report.getModel())));
        document.add(new Paragraph(String.format("Year of production: %s", report.getYearOfProduction())));
        document.add(new Paragraph(String.format("License plate: %s", report.getLicensePlate())));
        document.add(new Paragraph(String.format("VIN: %s", report.getVin())));
        document.add(new Paragraph("\n"));
    }

    private void addServices(Report report, Currency currency, double currentRate, double sum, Document document) throws DocumentException {
        document.add(new Paragraph("Services:", FontFactory.getFont(HELVETICA_BOLD)));
        document.add(new Paragraph("\n"));
        if (report.getReportServices() == null || report.getReportServices().isEmpty()) {
            String noServicesMessage = """
                    Our team of mechanics has examined your car and determined that no maintenance or additional\s
                    services need to be applied to your vehicle.

                    If you have any questions or concerns about the work we performed, please don't hesitate to reach\s
                    out on asg.car.service.system@gmail.com. We're always here to help and ensure your satisfaction.

                    Best regards,
                    ASG Car Service Team.""";
            document.add(new Paragraph(noServicesMessage));
        } else {
            PdfPTable table = new PdfPTable(2);
            table.addCell(new PdfPCell(new Paragraph("Service", FontFactory.getFont(HELVETICA_BOLD))));
            table.addCell(new PdfPCell(new Paragraph(String.format("Price(%s)", currency.getCurrencyCode()),
                    FontFactory.getFont(HELVETICA_BOLD))));

            for (ReportService rs : report.getReportServices()) {
                table.addCell(new PdfPCell(new Paragraph(rs.getService())));
                double currentPrice = converter.convertBgnTo(currentRate, rs.getPrice());
                table.addCell(new PdfPCell(new Paragraph(String.format("%.2f", currentPrice))));
                sum += currentPrice;
            }
            table.addCell(new PdfPCell(new Paragraph("Total:", FontFactory.getFont(HELVETICA_BOLD))));
            table.addCell(new PdfPCell(new Paragraph(String.format("%.2f %s", sum, currency.getCurrencyCode()),
                    FontFactory.getFont(HELVETICA_BOLD))));
            document.add(table);
        }
    }

    private String formatDate(LocalDateTime datetime) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy[ 'at' HH:mm:ss]", Locale.US);
        return fmt.format(datetime);
    }
}