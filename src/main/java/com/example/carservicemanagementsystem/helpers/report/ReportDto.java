package com.example.carservicemanagementsystem.helpers.report;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ReportDto {

    Integer reportId;
    String brand;
    String model;
    String licensePlate;
    LocalDateTime executionDate;
    LocalDateTime finishDate;
    int servicesNumber;
    double totalPrice;
}
