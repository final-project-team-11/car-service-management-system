package com.example.carservicemanagementsystem.helpers.visits;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ServiceDto {

    private Integer id;
    private String name;
    private boolean isChecked;
}
