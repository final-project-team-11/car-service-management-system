package com.example.carservicemanagementsystem.helpers.visits;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class EditVisitDto {

    private LocalDateTime executionDate;
    private Integer carId;
    private Integer statusId;
    private List<ServiceDto> services;
    private Set<Integer> serviceIds;
}
