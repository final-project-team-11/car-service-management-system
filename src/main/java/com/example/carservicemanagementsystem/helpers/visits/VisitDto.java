package com.example.carservicemanagementsystem.helpers.visits;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
public class VisitDto {
    @JsonProperty("execution_date")
    @DateTimeFormat(pattern = "dd-MMM-yyyy HH:mm")
    private LocalDateTime executionDate;
    @JsonProperty("carIdentificationType")
    private String carIdentificationType;
    @JsonProperty("carIdentificationValue")
    private String carIdentificationValue;
    @JsonProperty("status")
    private Integer status;

    @Override
    public String toString() {
        return "VisitDto{" +
                "carIdentificationType='" + carIdentificationType + '\'' +
                ", getCarIdentificationValue='" + carIdentificationValue + '\'' +
                '}';
    }
}