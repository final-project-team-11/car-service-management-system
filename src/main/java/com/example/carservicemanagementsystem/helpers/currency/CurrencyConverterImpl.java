package com.example.carservicemanagementsystem.helpers.currency;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.time.LocalDate;
import java.util.Currency;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class CurrencyConverterImpl implements CurrencyConverter {
    public double exchangeCurrency(String currency) {
        double result = 0.0;
        try {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(200, TimeUnit.SECONDS)
                    .readTimeout(200, TimeUnit.SECONDS)
                    .build();
            Request request = new Request.Builder()
                    .url("https://api.apilayer.com/exchangerates_data/"
                            + String.format(LocalDate.now().minusDays(1) + "?symbols=%s&base=BGN", currency))
                    .addHeader("apikey", "iD2UAvn66HdrzklNx3qnuwXrCIRLvyEd")
                    .method("GET", null).build();


            Response response = client.newCall(request).execute();

            assert response.body() != null;
            String jsonResponse = response.body().string();

            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode jsonNode = objectMapper.readTree(jsonResponse);
            String exchangeRate = jsonNode.get("rates").get(currency).asText();

            result = Double.parseDouble(exchangeRate);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public double convertBgnTo(double rate, double price) {
        return price * rate;
    }

    @Override
    public double getRate(Currency currency) {
        return exchangeCurrency(currency.getCurrencyCode());
    }
}
