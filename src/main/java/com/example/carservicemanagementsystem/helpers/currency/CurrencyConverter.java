package com.example.carservicemanagementsystem.helpers.currency;


import java.util.Currency;

public interface CurrencyConverter {
    double exchangeCurrency(String currency);

    double convertBgnTo(double rate, double price);

    double getRate(Currency currency);
}
