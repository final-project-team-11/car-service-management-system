package com.example.carservicemanagementsystem.helpers.email;

import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.User;
import org.springframework.core.io.FileSystemResource;

public interface EmailSenderService {
    void sendPdf(Report report, FileSystemResource file);

    void sendCredentials(User user);
}
