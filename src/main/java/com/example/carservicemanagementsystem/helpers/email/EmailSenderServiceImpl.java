package com.example.carservicemanagementsystem.helpers.email;

import com.example.carservicemanagementsystem.exceptions.EntityNotFoundException;
import com.example.carservicemanagementsystem.exceptions.SendingEmailException;
import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.User;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailSenderServiceImpl implements EmailSenderService {

    private final JavaMailSender mailSender;

    public void sendPdf(Report report, FileSystemResource file) {
        String email = report.getEmail();
        String firstName = report.getFirstName();
        String lastName = report.getLastName();

        if (file.getFilename() == null) {
            throw new EntityNotFoundException("No PDF found for the corresponding Report.");
        }

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(email);
            helper.setSubject("Report from the visit.");

            String body = String.format("<p>Hello, %s %s. "
                            + "This is a Report of your car's visit to our Smart Garage. In the document below you will see "
                            + "information about the servicing done, as well as the corresponding price for each.</p>", firstName,
                    lastName);
            helper.setText(body, true);
            helper.addAttachment(file.getFilename(), file);

            mailSender.send(message);
        } catch (Exception e) {
            throw new SendingEmailException("There was a problem sending the PDF to the email address.");
        }
    }

    public void sendCredentials(User user) {
        String email = user.getEmail();
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String username = user.getUsername();
        String password = user.getPassword();

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setTo(email);
            helper.setSubject("Login credentials");

            String body = String.format("<p>Dear %s %s,</p>"
                    + "<p>We are pleased to inform you that your account has been created successfully.</p>"
                    + "<p>Your username and password for accessing our website are provided below:</p>"
                    + "<br>"
                    + "<p>Username: %s%n<br>Password: %s%n</p>"
                    + "<p>Please keep these login credentials safe and confidential, as they are the key to accessing your account.</p>"
                    + "<p>We recommend that you change your password periodically to ensure the security of your account.</p>"
                    + "<br>"
                    + "<p>If you have any difficulty logging in or accessing your account, please do not hesitate" +
                    " to contact our support team at asg.car.service.system@gmail.com</p>."
                    + "<br>"
                    + "<p>We will be happy to assist you.</p>"
                    + "<br>"
                    + "<p>Thank you for choosing our service. We look forward to serving you.</p>"
                    + "<br>"
                    + "Best regards,"
                    + "<br>"
                    + "ASG Car Service.", firstName, lastName, username, password);
            helper.setText(body, true);

            mailSender.send(message);
        } catch (Exception e) {
            throw new SendingEmailException("An error occurred when sending the credentials.");
        }
    }
}


