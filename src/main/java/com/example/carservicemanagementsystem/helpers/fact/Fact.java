package com.example.carservicemanagementsystem.helpers.fact;

import com.example.carservicemanagementsystem.helpers.report.ReportDto;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.Service;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.CarService;
import com.example.carservicemanagementsystem.services.contracts.ReportService;
import com.example.carservicemanagementsystem.services.contracts.ServiceService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.util.List;

@Component
@RequiredArgsConstructor
public class Fact {
    private final UserService userService;
    private final CarService carService;
    private final ReportService reportService;
    private final ServiceService serviceService;

    public void factsBar(Model model) {

        List<Service> serviceList = serviceService.getAll();

        List<User> customerList = userService.getAll()
                .stream()
                .filter(u -> u.getRole().getId() == 1)
                .toList();

        List<User> employeeList = userService.getAll()
                .stream()
                .filter(u -> u.getRole().getId() == 2)
                .toList();

        List<Car> carList = carService.getCars(null, null, null);

        List<ReportDto> reportList = reportService.getReportDtosByParameters(null, null, null, null);

        model.addAttribute("services", serviceList);
        model.addAttribute("customers", customerList);
        model.addAttribute("employees", employeeList);
        model.addAttribute("cars", carList);
        model.addAttribute("reports", reportList);
    }
}
