package com.example.carservicemanagementsystem.helpers.validations;

import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Component
@RequiredArgsConstructor
public class ValidationHelperImpl implements ValidationHelper {
    private final UserService userService;

    @Override
    public void checkPhoneNumberForDuplicate(String phoneNumber, BindingResult bindingResult) {
        try {
            userService.getByPhoneNumber(phoneNumber);
        } catch (NotFoundException e) {
            return;
        }
        bindingResult.addError(new FieldError(bindingResult.getObjectName(), "phone_number",
                "This phone number is already taken."));
    }

    @Override
    public void checkEmailForDuplicate(String email, BindingResult bindingResult) {
        try {
            userService.getByEmail(email);
        } catch (NotFoundException e) {
            return;
        }
        bindingResult.addError(new FieldError(bindingResult.getObjectName(), "email",
                "This email address is already taken."));
    }

    @Override
    public void checkUsernameForDuplicate(String username, BindingResult bindingResult) {
        try {
            userService.getByUsername(username);
        } catch (NotFoundException e) {
            return;
        }
        bindingResult.addError(new FieldError(bindingResult.getObjectName(), "username",
                "This username is already taken."));
    }

    public void validatePhoneNumber(String phoneNumber, BindingResult bindingResult) {
        if (!phoneNumber.isBlank()) {
            try {
                if (phoneNumber.length() != 10) {
                    bindingResult.addError(new FieldError(bindingResult.getObjectName(), "phone_number",
                            "The phone number must be exactly 10 digits long."));
                }
                Long.parseLong(phoneNumber);

            } catch (NumberFormatException e) {
                bindingResult.addError(new FieldError(bindingResult.getObjectName(), "phone_number",
                        "The phone number must contain digits only."));
            }
        }
    }
}
