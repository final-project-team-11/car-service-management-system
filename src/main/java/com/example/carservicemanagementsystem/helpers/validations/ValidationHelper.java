package com.example.carservicemanagementsystem.helpers.validations;

import org.springframework.validation.BindingResult;

public interface ValidationHelper {
    void validatePhoneNumber(String phoneNumber, BindingResult bindingResult);

    void checkUsernameForDuplicate(String username, BindingResult bindingResult);

    void checkEmailForDuplicate(String email, BindingResult bindingResult);

    void checkPhoneNumberForDuplicate(String phoneNumber, BindingResult bindingResult);
}
