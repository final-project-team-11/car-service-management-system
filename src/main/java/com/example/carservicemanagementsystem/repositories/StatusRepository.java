package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface StatusRepository extends JpaRepository<Status, Integer> {

    @Query("select s from Status s where " +
            "(:statusId is null or s.statusId = :statusId) and " +
            "(:statusName is null or s.statusName like %:statusName%)")
    List<Status> getByStatusContain(Integer statusId, String statusName);


}
