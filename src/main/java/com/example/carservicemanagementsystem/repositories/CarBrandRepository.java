package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.CarBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarBrandRepository extends JpaRepository<CarBrand, Integer> {
}
