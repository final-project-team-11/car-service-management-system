package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.ReportService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportServiceRepository extends JpaRepository<ReportService, Integer> {

    @Query("select count(rs.report.reportId) from ReportService rs where rs.report.reportId = :reportId")
    Integer numberOfServices(Integer reportId);

    @Query("select rs from ReportService rs where rs.report.reportId = :reportId")
    List<ReportService> getByReportId(Integer reportId);
}
