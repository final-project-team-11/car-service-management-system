package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    @Query("select r from Role r where r.id = :id")
    Role getById(Integer id);

    @Query("select r from Role r where r.role = :roleName")
    Role getByName(String roleName);
}
