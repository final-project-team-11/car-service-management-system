package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.Visit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface VisitRepository extends JpaRepository<Visit, Integer> {
    @Query("select v from Visit v where " +
            "(:statusId is null or v.status.statusId = :statusId) and " +
            "(:statusName is null or v.status.statusName like %:statusName%)")
    List<Visit> getByStatus(Integer statusId, String statusName);

    @Query("select v from Visit v where " +
            "(:id is null or v.car.id = :id) and " +
            "(:licence is null or v.car.licencePlate like %:licence%) and " +
            "(:vin is null or v.car.vin like %:vin%) and " +
            "(:year is null or v.car.year = :year)")
    List<Visit> getByCar(Integer id, String licence, String vin, Integer year);

    @Query("SELECT v FROM Visit v JOIN v.services s WHERE s.serviceId = :serviceId")
    List<Visit> getByService(Integer serviceId);

    @Query("SELECT v FROM Visit v WHERE " +
            "(:dateType is null and :dateBefore is null and :dateAfter is null and :start is null and :end is null) OR " +
            "(:dateType = 'orderDate' AND v.orderDate BETWEEN :start AND :end) OR " +
            "(:dateType = 'executionDate' AND v.executionDate BETWEEN :start AND :end) OR " +
            "(:dateType = 'orderDate' AND v.orderDate < :dateBefore) OR " +
            "(:dateType = 'executionDate' AND v.executionDate < :dateBefore) OR " +
            "(:dateType = 'orderDate' AND v.orderDate > :dateAfter) OR " +
            "(:dateType = 'executionDate' AND v.executionDate > :dateAfter)" +
            "ORDER BY " +
            "CASE WHEN :dateType = 'orderDate' THEN v.orderDate ELSE v.executionDate END ASC")
    List<Visit> getVisitsByDate(String dateType, LocalDateTime start, LocalDateTime end,
                                LocalDateTime dateBefore, LocalDateTime dateAfter);

    List<Visit> findByVisitIdEquals(Integer id);

    @Query(value = "SELECT vs.* from visits vs " +
            "inner join status st on st.status_id = vs.status_id where " +
            "(:keyword IS NULL OR vs.order_date = :keyword) or " +
            "(:keyword IS NULL OR vs.car_id = :keyword) or " +
            "(:keyword IS NULL OR st.status_name like :keyword)",
            nativeQuery = true)
    List<Visit> findByKeyword(String keyword);
}
