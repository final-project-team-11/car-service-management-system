package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarModelRepository extends JpaRepository<CarModel, Integer> {

    CarModel findCarModelByModelName(String modelName);
}