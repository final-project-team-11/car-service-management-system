package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("select u from User u where u.id = :id")
    User getByUserId(Integer id);

    @Query("select u from User u where u.username = :username")
    User getByUsername(String username);

    @Query("select u from User u where u.email = :email")
    User getByEmail(String email);

    @Query("select u from User u where u.phoneNumber = :phoneNumber")
    User getByPhoneNumber(String phoneNumber);


    @Query("SELECT u FROM User u WHERE " +
            "(:id IS NULL OR u.id = :id) AND " +
            "(:username IS NULL OR u.username = :username) AND " +
            "(:phoneNumber IS NULL OR u.phoneNumber = :phoneNumber) AND " +
            "(:email IS NULL OR u.email = :email)")
    List<User> findUsersByAttributes(Integer id, String username, String phoneNumber, String email);


    @Query("SELECT u FROM User u WHERE " +
            "(:keyword IS NULL OR u.firstName like :keyword) or " +
            "(:keyword IS NULL OR u.lastName like :keyword) or " +
            "(:keyword IS NULL OR u.username like :keyword) or " +
            "(:keyword IS NULL OR u.email like :keyword)")
    List<User> findByKeyword(String keyword);
}
