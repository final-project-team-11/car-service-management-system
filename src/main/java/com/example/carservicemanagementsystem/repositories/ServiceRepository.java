package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ServiceRepository extends JpaRepository<Service, Integer> {
    @Query("select s from Service s where " +
            "(:id is null or s.serviceId = :id) and " +
            "(:serviceName is null or s.serviceName like %:serviceName%) and " +
            "((:priceBefore is null and :priceAfter is null) or " +
            "(:priceBefore is null and s.price > :priceAfter) or " +
            "(:priceAfter is null and s.price < :priceBefore) or " +
            "(s.price between :priceBefore and :priceAfter))")
    List<Service> getByServiceContain(Integer id, String serviceName, Double priceBefore, Double priceAfter);

}
