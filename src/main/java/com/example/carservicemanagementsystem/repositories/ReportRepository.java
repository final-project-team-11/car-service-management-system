package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ReportRepository extends JpaRepository<Report, Integer> {

    Report getByReportId(Integer reportId);

    @Query("SELECT r FROM Report r WHERE " +
            "((:after IS NULL AND :before IS NULL) OR " +
            "(r.executionDate = :after)) OR " +
            "((:after IS NULL AND r.executionDate <= :before) OR " +
            "(:before IS NULL AND r.executionDate >= :after)) OR" +
            "(r.executionDate BETWEEN :after and :before)")
    List<Report> findReportsByExecutionDate(@Param("after") LocalDateTime after,
                                            @Param("before") LocalDateTime before);

    @Query("SELECT r FROM Report r WHERE " +
            "((:after IS NULL AND :before IS NULL) OR " +
            "(r.finishDate = :after)) OR " +
            "((:after IS NULL AND r.finishDate <= :before) OR " +
            "(:before IS NULL AND r.finishDate >= :after)) OR" +
            "(r.executionDate BETWEEN :after and :before)")
    List<Report> findReportsByFinishDate(@Param("after") LocalDateTime after,
                                         @Param("before") LocalDateTime before);

    @Query("SELECT r FROM Report r WHERE " +
            "(:reportId IS NULL OR r.reportId = :reportId) AND " +
            "(:licensePlate IS NULL OR r.licensePlate = :licensePlate) AND " +
            "(:executionDate IS NULL OR r.executionDate = :executionDate) AND " +
            "(:finishDate IS NULL OR r.finishDate = :finishDate)")
    List<Report> findByParameters(@Param("reportId") Integer reportId,
                                  @Param("licensePlate") String licensePlate,
                                  @Param("executionDate") LocalDateTime executionDate,
                                  @Param("finishDate") LocalDateTime finishDate);

}

