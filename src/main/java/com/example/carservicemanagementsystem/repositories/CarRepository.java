package com.example.carservicemanagementsystem.repositories;

import com.example.carservicemanagementsystem.models.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {

    @Query("select car from Car car where car.licencePlate = :licencePlate")
    Car getByLicencePlate(String licencePlate);

    @Query("select car from Car car where car.vin = :vin")
    Car getByVin(String vin);

    @Query("select car from Car car where car.user.phoneNumber = :phoneNumber")
    List<Car> getByPhoneNumber(String phoneNumber);

    @Query("select car from Car car where " +
            "(:model is null or car.carModel.modelName = :model) and " +
            "(:brand is null or car.carModel.carBrand.brandName = :brand) and " +
            "(:yearOfProduction is null or car.year = :yearOfProduction)")
    List<Car> findCars(String model, String brand, Integer yearOfProduction);


    @Query("SELECT car FROM Car car WHERE " +
            "(:keyword IS NULL OR car.carModel.carBrand.brandName like :keyword) or " +
            "(:keyword IS NULL OR car.carModel.modelName like :keyword) or " +
            "(:keyword IS NULL OR car.vin like :keyword) or " +
            "(:keyword IS NULL OR car.year = :keyword) or " +
            "(:keyword IS NULL OR car.licencePlate = :keyword)")
    List<Car> findByKeyword(String keyword);
}