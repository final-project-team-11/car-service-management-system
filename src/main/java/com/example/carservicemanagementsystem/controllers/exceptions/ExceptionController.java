package com.example.carservicemanagementsystem.controllers.exceptions;

import com.example.carservicemanagementsystem.exceptions.*;
import com.example.carservicemanagementsystem.helpers.error.ErrorDto;
import jakarta.mail.MessagingException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

public abstract class ExceptionController {

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ErrorDto handleValidationError(MethodArgumentNotValidException e) {
        String errorMessage = e.getAllErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.joining(", "));

        ErrorDto errorDto = new ErrorDto();
        errorDto.setStatus(HttpStatus.BAD_REQUEST.value());
        errorDto.setMsg(errorMessage);
        errorDto.setTime(LocalDateTime.now());

        return errorDto;
    }

    @ExceptionHandler(value = NotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ErrorDto handleNotFound(Exception e) {
        return buildErrorInfo(e, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = DuplicateEntityException.class)
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public ErrorDto handleDuplicate(Exception e) {
        return buildErrorInfo(e, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = BadRequestException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ErrorDto handleBadRequest(Exception e) {
        return buildErrorInfo(e, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UnauthorizedException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ErrorDto handleUnauthorized(Exception e) {
        return buildErrorInfo(e, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = ForbiddenException.class)
    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    public ErrorDto handleAllOthers(ForbiddenException e) {
        return buildErrorInfo(e, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = ExecutionDateException.class)
    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    public ErrorDto handleAllOthers(ExecutionDateException e) {
        return buildErrorInfo(e, HttpStatus.FORBIDDEN);
    }


    @ExceptionHandler(value = SendingEmailException.class)
    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    public ErrorDto handleAllOthers(SendingEmailException e) {
        return buildErrorInfo(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleAllOthers(Exception e) {
        return buildErrorInfo(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ErrorDto buildErrorInfo(Exception e, HttpStatus status) {
        ErrorDto dto = new ErrorDto();
        dto.setStatus(status.value());
        dto.setMsg(e.getMessage());
        dto.setTime(LocalDateTime.now());
        return dto;
    }
}
