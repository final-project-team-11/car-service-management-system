package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.fact.Fact;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.CarService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.example.carservicemanagementsystem.controllers.mvc.HomeMvcController.DEFAULT_EMPLOYEE_ID;

@Controller
@RequestMapping("/bookingSelectCar")
@RequiredArgsConstructor
public class BookingSelectCarMvcController {
    private final Fact fact;
    private final CarService carService;
    private final UserService userService;

    @GetMapping()
    public String showBookingSelectCarPage(Model model, HttpSession session) {
        fact.factsBar(model);
        User viewer = (User) session.getAttribute("viewer");
        Car carSelect = (Car) session.getAttribute("carSelect");

        model.addAttribute("carSelect", carSelect);
        User ownerSelectCar = carSelect.getUser();
        model.addAttribute("ownerSelectCar", ownerSelectCar);


        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        if (!isAuthenticated) {
            return "404";
        } else {
            model.addAttribute("viewer", viewer);
            if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                return "bookingSelectCarEmployee";
            } else {
                return "bookingSelectCarCustom";
            }
        }
    }
}
