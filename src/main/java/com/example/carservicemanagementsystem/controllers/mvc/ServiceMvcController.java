package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.fact.Fact;
import com.example.carservicemanagementsystem.models.Service;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.ServiceService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static com.example.carservicemanagementsystem.controllers.mvc.HomeMvcController.DEFAULT_EMPLOYEE_ID;

@Controller
@RequestMapping("/services")
@RequiredArgsConstructor
public class ServiceMvcController {
    private final Fact fact;
    private final ServiceService serviceService;

    @GetMapping()
    public String showServicePage(Model model, HttpSession session) {
        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        if (!isAuthenticated) {
            return "serviceGuest";
        } else {
            model.addAttribute("viewer", viewer);
            if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                return "serviceEmployee";
            } else {
                return "serviceCustom";
            }
        }
    }

    @PostMapping("/create")
    public String createService(HttpServletRequest request, Model model, HttpSession session) {
        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        String serviceName = request.getParameter("serviceName");
        Double price = Double.valueOf(request.getParameter("price"));

        Service newService = new Service();
        newService.setServiceName(serviceName);
        newService.setPrice(price);

        serviceService.create(newService);

        if (!isAuthenticated) {
            return "404";
        } else {
            model.addAttribute("viewer", viewer);
            return "redirect:/services";
        }
    }

    @PutMapping("/modify")
    public String modifyService(HttpServletRequest request, Model model, HttpSession session) {
        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        int serviceId = Integer.parseInt(request.getParameter("serviceId"));
        String serviceName = request.getParameter("serviceName");
        Double price = Double.valueOf(request.getParameter("price"));

        Service service = serviceService.getById(serviceId);

        if (service == null) {
            return "404";
        }

        service.setServiceName(serviceName);
        service.setPrice(price);

        serviceService.update(serviceId, service);

        return "redirect:/services/";
    }

    @DeleteMapping("/delete/{serviceId}")
    public String deleteService(@PathVariable("serviceId") int serviceId) {
        // TODO: Implement the delete logic here
        return "redirect:/services";
    }
}
