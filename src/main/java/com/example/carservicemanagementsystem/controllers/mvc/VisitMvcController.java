package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.fact.Fact;
import com.example.carservicemanagementsystem.helpers.visits.EditVisitDto;
import com.example.carservicemanagementsystem.helpers.visits.ServiceDto;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.models.Visit;
import com.example.carservicemanagementsystem.services.contracts.ServiceService;
import com.example.carservicemanagementsystem.services.contracts.StatusService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import com.example.carservicemanagementsystem.services.contracts.VisitService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;


@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    public static final int DEFAULT_STATUS_ID = 1;
    private final VisitService visitService;
    private final UserService userService;
    private final ServiceService serviceService;
    private final StatusService statusService;
    private final Fact fact;

    @Autowired
    public VisitMvcController(VisitService visitService, UserService userService, ServiceService serviceService, StatusService statusService, Fact fact) {
        this.visitService = visitService;
        this.userService = userService;
        this.serviceService = serviceService;
        this.statusService = statusService;
        this.fact = fact;
    }

    @GetMapping("/view")
    public String visitsView(Model model, String keyword, HttpSession session) {
        User sessionUser = (User) session.getAttribute("viewer");
        User user = userService.getById(sessionUser.getId());
        model.addAttribute("viewer", sessionUser);
        model.addAttribute("owner", user);


        if (user.getRole().getRole().equalsIgnoreCase("customer")) {
            List<Visit> ownerVisits = user.getCars()
                    .stream()
                    .flatMap(car -> car.getVisits().stream())
                    .toList();
            model.addAttribute("visits", ownerVisits);
            return "myVisitsView";

        } else {
            List<Visit> visits = visitService.findByKeyword(keyword);
            model.addAttribute("visits", visits);
            return "allVisitsView";
        }
    }

    @GetMapping("/view/user/{id}")
    public String visitsView(@PathVariable Integer id, Model model, HttpSession session) {
        User owner = userService.getById(id);
        User viewer = (User) session.getAttribute("viewer");

        model.addAttribute("viewer", viewer);
        model.addAttribute("owner", owner);
        List<Visit> ownerVisits = owner.getCars()
                .stream()
                .flatMap(car -> car.getVisits().stream())
                .toList();
        model.addAttribute("visits", ownerVisits);
        if (ownerVisits.isEmpty()) {
            return "singleUserView";
        }

        return "myVisitsView";
    }

    @GetMapping("/{id}")
    private String showSingleVisitPage(@PathVariable Integer id, Model model, HttpSession session) {
        Visit visit = visitService.getById(id);
        User sessionUser = (User) session.getAttribute("viewer");
        User user = visit.getCar().getUser();
        model.addAttribute("viewer", sessionUser);
        model.addAttribute("owner", user);

        List<ServiceDto> serviceDtos = serviceService.getAll().stream()
                .map(service -> ServiceDto.builder()
                        .id(service.getServiceId())
                        .name(service.getServiceName())
                        .isChecked(visit.getServices().contains(service))
                        .build())
                .toList();

        EditVisitDto updatedVisitDto = new EditVisitDto();

        updatedVisitDto.setExecutionDate(visit.getExecutionDate());
        updatedVisitDto.setCarId(visit.getCar().getId());
        updatedVisitDto.setStatusId(visit.getStatus().getStatusId());
        updatedVisitDto.setServices(serviceDtos);
        model.addAttribute("id", visit.getVisitId());
        model.addAttribute("updateVisit", updatedVisitDto);
        return "singleVisitView";
    }

    @PostMapping()
    public String createVisit(HttpServletRequest request, Model model, HttpSession session) {
        fact.factsBar(model);
        User viewer = (User) session.getAttribute("viewer");

        String carId = request.getParameter("carId");
        String executionDateString = request.getParameter("executionDate");
        LocalDateTime executionDate = LocalDateTime.parse(executionDateString);

        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        Visit visitNew = new Visit();

        visitNew.setExecutionDate(executionDate);

        visitService.create(visitNew, "ID", carId);
        int visitId = visitNew.getVisitId();

        if (!isAuthenticated) {
            return "404";
        } else {
            model.addAttribute("viewer", viewer);
            model.addAttribute("visitId", visitId);
            return "redirect:/visits/" + visitId;
        }
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable Integer id, @ModelAttribute("updateVisit") EditVisitDto editVisitDto) {
        visitService.update(editVisitDto, id);
        return "redirect:/visits/view";
    }

    @PostMapping("/delete/{id}")
    public String deleteVisit(@PathVariable Integer id) {
        visitService.deleteById(id);
        return "redirect:/visits/view";
    }
}