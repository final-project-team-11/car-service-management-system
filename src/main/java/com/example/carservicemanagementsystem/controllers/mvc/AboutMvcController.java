package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.fact.Fact;
import com.example.carservicemanagementsystem.models.User;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.example.carservicemanagementsystem.controllers.mvc.HomeMvcController.DEFAULT_EMPLOYEE_ID;

@Controller
@RequestMapping("/about")
@RequiredArgsConstructor
public class AboutMvcController {
    private final Fact fact;

    @GetMapping()
    public String showAboutPage(Model model, HttpSession session) {
        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        if (!isAuthenticated) {
            return "aboutGuest";
        } else {
            model.addAttribute("viewer", viewer);
            if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                return "aboutEmployee";
            } else {
                return "aboutCustom";
            }
        }
    }
}
