package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.car.CreateCarDto;
import com.example.carservicemanagementsystem.helpers.car.UpdateCarDto;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.CarService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import com.example.carservicemanagementsystem.services.contracts.VisitService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/cars")
public class CarMvcController {

    private final CarService carService;
    private final UserService userService;
    private final VisitService visitService;

    @Autowired
    public CarMvcController(CarService carService, UserService userService, VisitService visitService) {
        this.carService = carService;
        this.userService = userService;
        this.visitService = visitService;
    }

    @GetMapping("user/{id}")
    public String createCarView(@PathVariable Integer id,HttpSession session, Model model) {
        User owner = userService.getById(id);
        User viewer = (User)session.getAttribute("viewer");
        model.addAttribute("owner", owner);
        model.addAttribute("viewer", viewer);
        model.addAttribute("create", new CreateCarDto());
        return "createCarView";
    }

    @PostMapping("/{id}")
    public String createCar(@ModelAttribute("create") @Valid CreateCarDto createCar,
                            BindingResult bindingResult, HttpSession session, Model model, @PathVariable Integer id) {

        User owner = userService.getById(id);
        User userSession = (User) session.getAttribute("viewer");
        userSession = userService.getById(userSession.getId());
        model.addAttribute("owner", owner);
        model.addAttribute("viewer", userSession);

        if (bindingResult.hasErrors()) {
            return "createCarView";
        }
        try {
            carService.create(createCar, owner);
            model.addAttribute("visits", visitService.getUserVisits(userSession));
            return "singleUserView";
        } catch (Exception e) {
            bindingResult.rejectValue("generalError", "", e.getMessage());
            return "createCarView";
        }
    }

    @GetMapping("/view")
    public String carsView(Model model, String keyword, HttpSession session) {
        User sessionUser = (User) session.getAttribute("viewer");
        User user = userService.getById(sessionUser.getId());

        if (user.getRole().getRole().equalsIgnoreCase("customer")) {
            List<Car> ownerCars = user.getCars();
            model.addAttribute("cars", ownerCars);
            model.addAttribute("viewer", sessionUser);
            model.addAttribute("owner", user);
            return "myCarsView";
        } else {
            List<Car> cars = carService.findByKeyword(keyword);
            model.addAttribute("cars", cars);
            return "allCarsView";
        }
    }
    @GetMapping("/view/user/{id}")
    public String showUserCars(@PathVariable Integer id ,
                               Model model,
                               HttpSession session){
        User owner = userService.getById(id);
        User viewer = (User)session.getAttribute("viewer");
        model.addAttribute("owner", owner);
        model.addAttribute("viewer", viewer);

        List<Car> ownerCars = new ArrayList<>(owner.getCars());
        if (ownerCars.isEmpty()){
            return "singleUserView";
        }
        model.addAttribute("cars", ownerCars);
        return "myCarsView";

    }

    @GetMapping("/{id}")
    private String showSingleCarPage(@PathVariable Integer id, Model model) {
        Car car = carService.getById(id);
        UpdateCarDto updateCarDto = new UpdateCarDto();
        updateCarDto.setBrand(car.getCarModel().getCarBrand().getBrandName());
        updateCarDto.setModel(car.getCarModel().getModelName());
        updateCarDto.setYear(car.getYear());
        updateCarDto.setVin(car.getVin());
        updateCarDto.setLicencePlate(car.getLicencePlate());
        model.addAttribute("id", car.getId());
        model.addAttribute("updateCar", updateCarDto);
        return "singleCarView";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable Integer id, @ModelAttribute("updateCar")
    UpdateCarDto updateCarDto, Model model) {
        Car updatedCar = carService.update(updateCarDto, id);
        model.addAttribute("updatedCar", updatedCar);
        return "redirect:/cars/view";
    }
}