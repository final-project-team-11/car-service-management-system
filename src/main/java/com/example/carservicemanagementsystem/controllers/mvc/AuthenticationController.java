package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.helpers.fact.Fact;
import com.example.carservicemanagementsystem.helpers.user.LoginDto;
import com.example.carservicemanagementsystem.helpers.user.UserCreateDto;
import com.example.carservicemanagementsystem.helpers.user.UserGenerator;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import static com.example.carservicemanagementsystem.controllers.mvc.HomeMvcController.DEFAULT_EMPLOYEE_ID;

@Controller
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserGenerator userGenerator;
    private final Fact fact;

    @GetMapping("/login")
    public String showAuthenticationPage(Model model, HttpSession session) {
        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);
        model.addAttribute("login", new LoginDto());
        model.addAttribute("register", new UserCreateDto());

        if (!isAuthenticated) {
            return "loginGuest";
        } else {
            model.addAttribute("viewer", viewer);
            if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                return "registrationFormEmployee";
            } else {
                return "404";
            }
        }
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login, BindingResult bindingResult,
                              HttpSession session, Model model) {
        fact.factsBar(model);
        if (bindingResult.hasErrors()) {
            return "loginGuest";
        }
        try {
            User user = authenticationHelper.validateUser(login.getUsername(), login.getPassword());

            session.setAttribute("viewer", user);
            return "redirect:/";
        } catch (Exception e) {
            bindingResult.rejectValue("invalidCredentials", "auth_error", e.getMessage());
            return "redirect:/auth/login";
        }
    }


    @PostMapping("/register")
    public String createUser(@Valid @ModelAttribute("register") UserCreateDto userCreateDto, BindingResult bindingResult,
                             HttpSession session, Model model) {

        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        if (bindingResult.hasErrors()) {
            return "redirect:/auth/register";
        }
        try {
            if (!isAuthenticated) {
                userCreateDto.setRole_id(1);

                User newUser = userGenerator.generateUser(userCreateDto);
                userService.createUser(newUser);

                return "redirect:/";
            } else {
                userCreateDto.setRole_id(1);

                User newUser = userGenerator.generateUser(userCreateDto);
                userService.createUser(newUser);
                return "homePageEmployee";
            }
        } catch (Exception e) {
            bindingResult.rejectValue("invalidCredentials", "auth_error", e.getMessage());
            return "redirect:/auth/login";
        }


    }


    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.invalidate();
        return "redirect:/";
    }

    @GetMapping("/forgotten-password")
    public String forgottenPasswordForm() {
        return "redirect:/";
    }
}