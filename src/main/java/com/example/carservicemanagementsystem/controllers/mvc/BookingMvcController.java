package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.fact.Fact;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.CarService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.carservicemanagementsystem.controllers.mvc.HomeMvcController.DEFAULT_EMPLOYEE_ID;

@Controller
@RequestMapping("/booking")
@RequiredArgsConstructor
public class BookingMvcController {
    private final Fact fact;
    private final CarService carService;

    @GetMapping()
    public String showBookingPage(Model model, HttpSession session) {
        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);
        List<Car> userCars = carService.getAll().stream()
                .filter(car -> car.getUser().getId().equals(viewer.getId()))
                .collect(Collectors.toList());

        model.addAttribute("carList", userCars);

        if (!isAuthenticated) {
            return "404";
        } else {
            model.addAttribute("viewer", viewer);
            if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                return "bookingEmployee";
            } else {
                return "bookingCustom";
            }
        }
    }

    @GetMapping("/visit")
    public String bookUserVisit(Model model, HttpSession session) {
        fact.factsBar(model);
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("viewer", viewer);
        List<Car> userCars = carService.getAll().stream()
                .filter(car -> car.getUser().getId().equals(viewer.getId()))
                .collect(Collectors.toList());

        model.addAttribute("carList", userCars);
        return "bookingCustom";
    }

    @GetMapping("/selectCar")
    public String selectCar(@RequestParam(required = false) String vin,
                            @RequestParam(required = false) String licencePlate,
                            Model model, HttpSession session) {
        fact.factsBar(model);
        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);
        model.addAttribute("viewer", viewer);
        Car carSelect = new Car();

        if (vin != null && !vin.isEmpty()) {
            carSelect = carService.getByVin(vin);
        } else if (licencePlate != null && !licencePlate.isEmpty()) {
            carSelect = carService.getByLicencePlate(licencePlate);
        }
        if (carSelect != null) {
            model.addAttribute("carSelect", carSelect);
            if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                return "bookingSelectCarEmployee";
            } else {
                return "bookingSelectCarCustom";
            }
        } else {
            model.addAttribute("errorMessage", "Car not found");
            return "bookingSelectCarErrorEmployee";
        }
    }

    @GetMapping("/selectCarUser")
    public String selectCarUser(@RequestParam(required = false) String selected, Model model, HttpSession session) {


        try {
            fact.factsBar(model);
            User viewer = (User) session.getAttribute("viewer");
            boolean isAuthenticated = viewer != null;
            model.addAttribute("isAuthenticated", isAuthenticated);
            model.addAttribute("viewer", viewer);
            model.addAttribute("owner", viewer);

            if (selected == null || selected.isBlank()) {
                return "redirect:/booking/visit";
            }

            Car carSelect = carService.getByLicencePlate(selected);

            if (carSelect != null && viewer != null) {
                model.addAttribute("carSelect", carSelect);
                if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                    return "bookingSelectCarEmployee";
                } else {
                    return "bookingSelectCarCustom";
                }
            } else {
                model.addAttribute("errorMessage", "Car not found");
                return "bookingSelectCarErrorEmployee";
            }
        } catch (Exception e) {
            return "redirect:/booking/visit";
        }
    }
}
