package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.fact.Fact;
import com.example.carservicemanagementsystem.models.User;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
@RequiredArgsConstructor
public class HomeMvcController {
    public static final int DEFAULT_EMPLOYEE_ID = 2;
    private final Fact fact;

    @GetMapping()
    public String showAHomePage(Model model, HttpSession session) {
        fact.factsBar(model);

        User viewer = (User) session.getAttribute("viewer");
        boolean isAuthenticated = viewer != null;
        model.addAttribute("isAuthenticated", isAuthenticated);

        if (!isAuthenticated) {
            return "homePageGuest";
        } else {
            model.addAttribute("viewer", viewer);
            if (viewer.getRole().getId() == DEFAULT_EMPLOYEE_ID) {
                return "homePageEmployee";
            } else {
                return "homePageCustom";
            }
        }
    }
}