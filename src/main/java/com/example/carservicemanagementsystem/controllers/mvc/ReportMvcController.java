package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.ReportService;
import jakarta.servlet.http.HttpSession;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/reports")
@RequiredArgsConstructor
public class ReportMvcController {
    private final ReportService reportService;

    @ModelAttribute("reports")
    public List<Report> getAllReports(HttpSession session, Model model) {
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("viewer", viewer);
        return reportService.getAll();
    }

    @GetMapping
    public String viewAllReports(Model model, HttpSession session) {
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("viewer", viewer);
        if (reportService.getAll().isEmpty()) {
            return "redirect:/";
        }
        return "reportsView";
    }

    @GetMapping("/{id}")
    public String viewReportById(@PathVariable Integer id, Model model) {
        Report report = reportService.getById(id);
        Double total = report.getReportServices().stream()
                .map(com.example.carservicemanagementsystem.models.ReportService::getPrice)
                .mapToDouble(Double::doubleValue).sum();
        model.addAttribute("total", total);
        model.addAttribute("report", report);
        return "singleReportView";
    }

    @PostMapping("/send/{id}")
    public String sendToOwnerEmail(@PathVariable Integer id,
                                   @RequestParam(name = "currency") String currency) {
        try {
            reportService.sendReportToCustomer(id, currency);
        } catch (Exception e) {
            return "redirect:/reports/" + id;
        }
        return "redirect:/reports";
    }
}