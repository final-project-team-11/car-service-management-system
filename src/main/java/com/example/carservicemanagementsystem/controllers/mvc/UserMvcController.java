package com.example.carservicemanagementsystem.controllers.mvc;

import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.helpers.user.UserModifyDto;
import com.example.carservicemanagementsystem.helpers.validations.ValidationHelper;
import com.example.carservicemanagementsystem.models.CarBrand;
import com.example.carservicemanagementsystem.models.Report;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.*;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserMvcController {

    private final ValidationHelper validationHelper;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final VisitService visitService;
    private final CarBrandService carBrandService;
    private final ReportService reportService;

    @ModelAttribute("users")
    public List<User> modelUsers() {
        return userService.getAll();
    }

    @ModelAttribute("brands")
    public List<CarBrand> modelCars() {
        return carBrandService.getAll();
    }

    @GetMapping
    public String viewOfUsers(HttpSession session, Model model) {
        if (authenticationHelper.checkIfAuthenticated(session)) {
            return "redirect:/";
        }
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("viewer", viewer);
        return "usersView";
    }

    @PostMapping("/search")
    public String resultUsers(@RequestParam(name = "input", required = false) String query,
                              @RequestParam(name = "brandType", required = false) Integer brandId,
                              @RequestParam(name = "order", required = false) String order,
                              @RequestParam(name = "datepicker", required = false) LocalDate dateOne,
                              @RequestParam(name = "datepicker2", required = false) LocalDate dateTwo,
                              HttpSession session,
                              Model model) {
        if (authenticationHelper.checkIfAuthenticated(session)) {
            return "redirect:/";
        }
        User viewer = (User) session.getAttribute("viewer");
        List<User> users = userService.getFromInput(query, brandId, order, dateOne, dateTwo);
        model.addAttribute("users", users);
        model.addAttribute("viewer", viewer);
        return "usersView";
    }

    @GetMapping("/{id}")
    public String singleUser(@PathVariable Integer id, Model model, HttpSession session) {
        if (authenticationHelper.checkIfAuthenticated(session)) {
            return "redirect:/";
        }
        User owner = userService.getById(id);
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("owner", owner);
        model.addAttribute("viewer", viewer);
        model.addAttribute("visits", visitService.getUserVisits(owner));
        return "singleUserView";
    }


    @GetMapping("/edit/{id}")
    public String editUser(@PathVariable Integer id, HttpSession session, Model model) {
        if (authenticationHelper.checkIfAuthenticated(session)) {
            return "redirect:/";
        }
        User owner = userService.getById(id);
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("owner", owner);
        model.addAttribute("viewer", viewer);
        model.addAttribute("edit", new UserModifyDto());
        return "editUserView";
    }

    @PostMapping("/modify/{id}")
    public String modifyUser(@PathVariable Integer id,
                             @Valid @ModelAttribute("edit") UserModifyDto modifyDto,
                             BindingResult bindingResult,
                             HttpSession session,
                             Model model) {
        if (authenticationHelper.checkIfAuthenticated(session)) {
            return "redirect:/";
        }

        User owner = userService.getById(id);
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("owner", owner);
        model.addAttribute("viewer", viewer);

        validationHelper.validatePhoneNumber(modifyDto.getPhone_number(), bindingResult);
        if (bindingResult.hasErrors()) {
            return "editUserView";
        }
        validationHelper.checkEmailForDuplicate(modifyDto.getEmail(), bindingResult);
        validationHelper.checkUsernameForDuplicate(modifyDto.getUsername(), bindingResult);
        validationHelper.checkPhoneNumberForDuplicate(modifyDto.getPhone_number(), bindingResult);

        if (bindingResult.hasErrors()) {
            return "editUserView";
        }
        userService.updateUser(owner, modifyDto, bindingResult);
        if (viewer.getId().equals(owner.getId())) {
            viewer.setRole(owner.getRole());
        }
        if (viewer.getRole().getId().equals(1)) {
            session.invalidate();
            return "redirect:/";
        }
        return "editUserView";
    }

    @PostMapping("/delete/{id}")
    public String deleteUser(@PathVariable Integer id, HttpSession session, Model model) {
        if (authenticationHelper.checkIfAuthenticated(session)) {
            return "redirect:/";
        }
        User owner = userService.getById(id);
        User viewer = (User) session.getAttribute("viewer");
        if (owner.getId().equals(viewer.getId())) {
            session.removeAttribute("viewer");
            session.setAttribute("isAuthenticated", false);
            userService.deleteUser(owner);
            return "redirect:/";
        }
        model.addAttribute("viewer", viewer);
        userService.deleteUser(owner);
        return "redirect:/users";
    }

    @GetMapping("/reports/{id}")
    public String getUserReports(@PathVariable Integer id,
                                 HttpSession session,
                                 Model model) {
        if (authenticationHelper.checkIfAuthenticated(session)) {
            return "redirect:/";
        }
        User owner = userService.getById(id);
        User viewer = (User) session.getAttribute("viewer");
        model.addAttribute("owner", owner);
        model.addAttribute("viewer", viewer);
        List<Report> reports = reportService.getAll().stream()
                .filter(r -> r.getUsername().equals(owner.getUsername()))
                .collect(Collectors.toList());
        model.addAttribute("reports", reports);
        if (!reports.isEmpty()) {
            return "userReportsView";
        }
        return "redirect:/users/" + id;
    }
}