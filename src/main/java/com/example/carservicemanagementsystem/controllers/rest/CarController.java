package com.example.carservicemanagementsystem.controllers.rest;

import com.example.carservicemanagementsystem.controllers.exceptions.ExceptionController;
import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.helpers.car.CreateCarBrandDto;
import com.example.carservicemanagementsystem.helpers.car.CreateCarDto;
import com.example.carservicemanagementsystem.helpers.car.CreateCarModelDto;
import com.example.carservicemanagementsystem.helpers.car.UpdateCarDto;
import com.example.carservicemanagementsystem.models.Car;
import com.example.carservicemanagementsystem.models.CarBrand;
import com.example.carservicemanagementsystem.models.CarModel;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.CarBrandService;
import com.example.carservicemanagementsystem.services.contracts.CarModelService;
import com.example.carservicemanagementsystem.services.contracts.CarService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/cars")
@RequiredArgsConstructor
public class CarController extends ExceptionController {

    private final CarService carService;
    private final CarModelService modelService;
    private final CarBrandService brandService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;


    @GetMapping("/{id}")
    public Car getById(@PathVariable Integer id) {
        return carService.getById(id);
    }

    @GetMapping("/licencePlate")
    public Car getByLicencePlate(@RequestParam("licencePlate") String licencePlate) {
        return carService.getByLicencePlate(licencePlate);
    }

    @GetMapping("/vin")
    public Car getByVin(@RequestParam("vin") String vin) {
        return carService.getByVin(vin);
    }

    @GetMapping("/user/phone")
    public List<Car> getByPhoneNumber(@RequestParam("phone") String phone) {
        return carService.getByPhoneNumber(phone);
    }

    @GetMapping
    public List<Car> filterCars(
            @RequestParam(required = false) String model,
            @RequestParam(required = false) String brand,
            @RequestParam(required = false) Integer yearOfProduction
    ) {
        return carService.getCars(model, brand, yearOfProduction);
    }

    @PutMapping("/{id}")
    public Car update(@RequestHeader HttpHeaders headers,
                      @RequestBody UpdateCarDto car,
                      @PathVariable Integer id) {

        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        return carService.update(car, id);
    }

    @PostMapping("/user/{id}")
    public String create(@RequestHeader HttpHeaders headers,
                         @RequestBody CreateCarDto car, @PathVariable Integer id) {
        User executor = authenticationHelper.tryGetUser(headers);
        User owner = userService.getById(id);
        userService.checkIfUserIsAuthorized(executor);
        carService.create(car, owner);
        return "Car is created";
    }

    @PostMapping("/models")
    public CarModel create(@RequestHeader HttpHeaders headers,
                           @RequestBody CreateCarModelDto model) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        return modelService.create(model);
    }

    @PostMapping("/brands")
    public CarBrand create(@RequestHeader HttpHeaders headers,
                           @RequestBody CreateCarBrandDto brand) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        return brandService.create(brand);
    }
}