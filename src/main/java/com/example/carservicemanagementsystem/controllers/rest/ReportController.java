package com.example.carservicemanagementsystem.controllers.rest;

import com.example.carservicemanagementsystem.controllers.exceptions.ExceptionController;
import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.helpers.report.ReportDto;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.ReportService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("api/reports")
@RequiredArgsConstructor
public class ReportController extends ExceptionController {

    private final UserService userService;
    private final ReportService reportService;
    private final AuthenticationHelper authenticationHelper;

    @GetMapping("/date")
    public List<ReportDto> getReports(@RequestParam(value = "date", required = false) String date,
                                      @RequestParam(value = "after", required = false)
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime after,
                                      @RequestParam(value = "before", required = false)
                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime before) {

        return reportService.getByDatetime(date, after, before);
    }

    @GetMapping
    public List<ReportDto> getReportsInfo(@RequestParam(value = "report_id", required = false) Integer report_id,
                                          @RequestParam(value = "license_plate", required = false) String licensePlate,
                                          @RequestParam(value = "execution_date", required = false)
                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                          LocalDateTime executionDate,
                                          @RequestParam(value = "finish_date", required = false)
                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                          LocalDateTime finishDate) {
        return reportService.getReportDtosByParameters(report_id, licensePlate, executionDate, finishDate);
    }

    @GetMapping("/send/{id}")
    public String sendToEmail(@RequestHeader HttpHeaders headers,
                              @PathVariable Integer id,
                              @RequestParam(value = "currency", required = false) String currency) {

        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        reportService.sendReportToCustomer(id, currency);
        return "Report has been sent to the owner's email";
    }
}
