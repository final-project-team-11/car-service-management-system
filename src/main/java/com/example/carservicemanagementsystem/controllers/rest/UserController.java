package com.example.carservicemanagementsystem.controllers.rest;

import com.example.carservicemanagementsystem.controllers.exceptions.ExceptionController;
import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.services.contracts.CarService;
import org.springframework.http.HttpHeaders;
import com.example.carservicemanagementsystem.helpers.user.UserGenerator;
import com.example.carservicemanagementsystem.helpers.user.UserCreateDto;
import com.example.carservicemanagementsystem.helpers.user.UserPasswordChangeDto;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController extends ExceptionController {

    private final UserService userService;
    private final UserGenerator userGenerator;
    private final CarService carService;
    private final AuthenticationHelper authenticationHelper;

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/attributes")
    public List<User> getByAttributes(@RequestParam(value = "id", required = false) Integer userId,
                                      @RequestParam(value = "username", required = false) String username,
                                      @RequestParam(value = "phone_number", required = false) String phoneNumber,
                                      @RequestParam(value = "email", required = false) String email) {

        return userService.getByAttributes(userId, username, phoneNumber, email);
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable Integer id) {
        return userService.getById(id);
    }

    @GetMapping("/username")
    public User getByUsername(@RequestParam("q") String username) {
        return userService.getByUsername(username);
    }

    @GetMapping("/phone")
    public User getByPhoneNumber(@RequestParam("q") String phoneNumber) {
        return userService.getByPhoneNumber(phoneNumber);
    }

    @GetMapping("/email")
    public User getByEmail(@RequestParam("q") String email) {
        return userService.getByEmail(email);
    }

    @GetMapping("/cars/license_plate")
    public User getCarOwner(@RequestParam("q") String number) {
        return carService.getByLicencePlate(number).getUser();
    }

    @GetMapping("/cars/vin")
    public User getUserByCarVin(@RequestParam(value = "q") @Min(17) @Max(17) String vin) {
        return carService.getByVin(vin).getUser();
    }

    @PostMapping
    public void createUser(@RequestHeader HttpHeaders headers,
                           @Valid @RequestBody UserCreateDto userCreateDto) {
        User executor = authenticationHelper.tryGetUser(headers);
        User newUser = userGenerator.generateUser(userCreateDto);
        userService.checkIfUserIsAuthorized(executor);
        userService.createUser(newUser);
    }

    @PutMapping("/{id}")
    public void updateUser(@RequestHeader HttpHeaders headers,
                           @PathVariable Integer id,
                           @RequestParam(value = "first_name", required = false) @Max(32) Optional<String> firstName,
                           @RequestParam(value = "last_name", required = false) @Max(32) Optional<String> lastName,
                           @RequestParam(value = "email", required = false) @Email Optional<String> email,
                           @RequestParam(value = "username", required = false) @Min(2) @Max(20) Optional<String> username,
                           @RequestParam(value = "phone_number", required = false) @Min(10) @Max(10) Optional<String> phoneNumber,
                           @RequestParam(value = "role_id", required = false) Optional<Integer> roleId) {
        User executor = authenticationHelper.tryGetUser(headers);
        User user = userService.getById(id);
        if (!executor.getId().equals(user.getId())) {
            userService.checkIfUserIsAuthorized(executor);
        }
        userService.updateUser(user, firstName, lastName, email, username, phoneNumber, roleId);
    }

    @PutMapping("/{id}/password")
    public void changePassword(@RequestHeader HttpHeaders headers,
                               @PathVariable Integer id,
                               @Valid @RequestBody UserPasswordChangeDto passDto) {
        User executor = authenticationHelper.tryGetUser(headers);
        User user = userService.getById(id);
        if (!user.getId().equals(executor.getId())) {
            userService.checkIfUserIsAuthorized(executor);
        }
        userService.changeUserPassword(user, passDto);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        User executor = authenticationHelper.tryGetUser(headers);
        User user = userService.getById(id);
        if (!executor.getId().equals(user.getId())) {
            userService.checkIfUserIsAuthorized(executor);
        }
        userService.deleteUser(user);
    }
}
