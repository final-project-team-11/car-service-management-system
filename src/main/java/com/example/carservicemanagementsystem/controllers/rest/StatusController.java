package com.example.carservicemanagementsystem.controllers.rest;

import com.example.carservicemanagementsystem.controllers.exceptions.ExceptionController;
import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.models.Status;
import com.example.carservicemanagementsystem.services.contracts.StatusService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/status")
@RequiredArgsConstructor
public class StatusController extends ExceptionController {
    private final UserService userService;
    private final StatusService statusService;
    private final AuthenticationHelper authenticationHelper;

    @GetMapping
    public List<Status> getAll() {
        return statusService.getAll();
    }

    @GetMapping("/{id}")
    public Status getById(@PathVariable Integer id) {
        return statusService.getById(id);
    }

    @GetMapping("/search")
    List<Status> getByStatusContain(
            @RequestParam(required = false) Integer statusId,
            @RequestParam(required = false) String statusName) {
        return statusService.getByStatusContain(statusId, statusName);
    }

    @PostMapping
    public Status createStatus(@RequestHeader HttpHeaders headers,
                               @RequestBody Status status) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);

        return statusService.create(status);
    }

    @DeleteMapping("/{id}")
    public void deleteStatus(@RequestHeader HttpHeaders headers,
                             @PathVariable Integer id) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        statusService.deleteById(id);
    }
}