package com.example.carservicemanagementsystem.controllers.rest;

import com.example.carservicemanagementsystem.controllers.exceptions.ExceptionController;
import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.helpers.visits.EditVisitDto;
import com.example.carservicemanagementsystem.helpers.visits.VisitDto;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.models.Visit;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import com.example.carservicemanagementsystem.services.contracts.VisitService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/visits")
@RequiredArgsConstructor
public class VisitController extends ExceptionController {

    private final UserService userService;
    private final VisitService visitService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @GetMapping
    public List<Visit> getAll() {
        return visitService.getAll();
    }

    @GetMapping("/{id}")
    public Visit getById(@PathVariable Integer id) {
        return visitService.getById(id);
    }

    @GetMapping("/status")
    List<Visit> getByStatus(
            @RequestParam(required = false) Integer statusId,
            @RequestParam(required = false) String statusName) {

        return visitService.getByStatus(statusId, statusName);
    }

    @GetMapping("/services")
    List<Visit> getByService(
            @RequestParam(required = false) Integer serviceId) {

        return visitService.getByServices(serviceId);
    }

    @GetMapping("/cars")
    List<Visit> getByCar(
            @RequestParam(required = false) Integer id,
            @RequestParam(required = false) String licence,
            @RequestParam(required = false) String vin,
            @RequestParam(required = false) Integer year) {

        return visitService.getByCar(id, licence, vin, year);
    }

    @GetMapping("/date")
    List<Visit> getByDate(
            @RequestParam(required = false) String dateType,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime start,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime end,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateBefore,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateAfter) {

        return visitService.getByDate(dateType, start, end, dateBefore, dateAfter);
    }

    @PostMapping()
    public Visit createVisit(@RequestHeader HttpHeaders headers,
                             @RequestBody VisitDto visitDto) {

        String carType = visitDto.getCarIdentificationType();
        String carValue = visitDto.getCarIdentificationValue();
        Visit newVisit = modelMapper.map(visitDto, Visit.class);
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        return visitService.create(newVisit, carType, carValue);
    }

    @PostMapping("/{visitId}/services/{serviceId}")
    public Visit addServiceToVisit(@RequestHeader HttpHeaders headers,
                                   @PathVariable Integer visitId,
                                   @PathVariable Integer serviceId) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);

        return visitService.addServiceToVisit(visitId, serviceId);
    }

    @DeleteMapping("/{visitId}/services/{serviceId}")
    public void deleteServiceFromVisit(@RequestHeader HttpHeaders headers,
                                       @PathVariable Integer visitId,
                                       @PathVariable Integer serviceId) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        visitService.deleteServiceFromVisit(visitId, serviceId);
    }

    @PutMapping("/{id}")
    public Visit updateVisit(@RequestHeader HttpHeaders headers,
                             @PathVariable Integer id,
                             @RequestBody EditVisitDto editVisitDto) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);

        return visitService.update(editVisitDto, id);
    }

    @DeleteMapping("/{id}")
    public void deleteVisitById(@RequestHeader HttpHeaders headers,
                                @PathVariable Integer id) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        visitService.deleteById(id);
    }

    @DeleteMapping("/status")
    public void deleteVisitByStatus(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false) Integer statusId,
            @RequestParam(required = false) String statusName) {

        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        visitService.deleteByStatus(statusId, statusName);
    }

    @DeleteMapping("/cars")
    public void deleteVisitByCar(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false) Integer id,
            @RequestParam(required = false) String licence,
            @RequestParam(required = false) String vin,
            @RequestParam(required = false) Integer year) {

        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        visitService.deleteByCar(id, licence, vin, year);
    }

    @DeleteMapping("/date")
    public void deleteVisitByDate(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false) String dateType,
            @RequestParam(required = false) LocalDateTime start,
            @RequestParam(required = false) LocalDateTime end,
            @RequestParam(required = false) LocalDateTime dateBefore,
            @RequestParam(required = false) LocalDateTime dateAfter) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        visitService.deleteByDate(dateType, start, end, dateBefore, dateAfter);
    }
}