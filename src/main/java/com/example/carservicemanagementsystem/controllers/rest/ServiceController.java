package com.example.carservicemanagementsystem.controllers.rest;

import com.example.carservicemanagementsystem.controllers.exceptions.ExceptionController;
import com.example.carservicemanagementsystem.helpers.authentications.AuthenticationHelper;
import com.example.carservicemanagementsystem.models.Service;
import com.example.carservicemanagementsystem.models.User;
import com.example.carservicemanagementsystem.services.contracts.ServiceService;
import com.example.carservicemanagementsystem.services.contracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/services")
@RequiredArgsConstructor
public class ServiceController extends ExceptionController {

    private final UserService userService;
    private final ServiceService serviceService;
    private final AuthenticationHelper authenticationHelper;

    @GetMapping
    public List<Service> getAll() {
        return serviceService.getAll();
    }

    @GetMapping("/{id}")
    public Service getById(@PathVariable Integer id) {
        return serviceService.getById(id);
    }

    @GetMapping("/search")
    public List<Service> getByServiceContain(
            @RequestParam(required = false) Integer id,
            @RequestParam(required = false) String serviceName,
            @RequestParam(required = false) Double priceBefore,
            @RequestParam(required = false) Double priceAfter) {

        return serviceService.getByServiceContain(id, serviceName, priceBefore, priceAfter);
    }

    @PostMapping
    public Service createService(@RequestHeader HttpHeaders headers,
                                 @RequestBody Service service) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        return serviceService.create(service);
    }

    @PutMapping("/{id}")
    public Service updateService(@RequestHeader HttpHeaders headers,
                                 @PathVariable Integer id,
                                 @RequestBody Service service) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        return serviceService.update(id, service);
    }

    @DeleteMapping("/{id}")
    public void deleteService(@RequestHeader HttpHeaders headers,
                              @PathVariable Integer id) {
        User executor = authenticationHelper.tryGetUser(headers);
        userService.checkIfUserIsAuthorized(executor);
        serviceService.deleteById(id);
    }
}
