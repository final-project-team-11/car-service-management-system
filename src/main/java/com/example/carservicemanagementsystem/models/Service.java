package com.example.carservicemanagementsystem.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "services")
@Getter
@Setter
@EqualsAndHashCode
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_id")
    private Integer serviceId;

    @NotBlank(message = "Service name cannot be empty!")
    @Size(min = 2, max = 32, message = "Service name must be between 2 and 32 symbols.")
    @Column(name = "service_name")
    private String serviceName;

    @Positive(message = "The price of the service must be positive.")
    @Column(name = "price")
    private double price;

    @ManyToMany(mappedBy = "services", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Visit> visits = new ArrayList<>();
}