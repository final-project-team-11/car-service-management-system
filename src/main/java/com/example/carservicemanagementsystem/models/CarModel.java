package com.example.carservicemanagementsystem.models;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "car_models")
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_model_id")
    private Integer id;
    @Column(name = "model_name")
    private String modelName;

    @ManyToOne
    @JoinColumn(name = "car_brand_id")
    private CarBrand carBrand;
}