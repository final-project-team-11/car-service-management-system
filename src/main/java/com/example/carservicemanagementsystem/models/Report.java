package com.example.carservicemanagementsystem.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "reports")
@Getter
@Setter
public class Report {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "report_id")
    private Integer reportId;

    @Column(name = "visit_id")
    private Integer visitId;

    @Column(name = "customer_username")
    private String username;
    @Column(name = "customer_first_name")
    private String firstName;

    @Column(name = "customer_email")
    private String email;
    @Column(name = "customer_last_name")
    private String lastName;

    @Column(name = "brand")
    private String brand;

    @Column(name = "model")
    private String model;

    @Column(name = "year_of_production")
    private Integer yearOfProduction;

    @Column(name = "vin")
    private String vin;

    @Column(name = "license_plate")
    private String licensePlate;

    @Column(name = "execution_date")
    private LocalDateTime executionDate;

    @Column(name = "finish_date")
    private LocalDateTime finishDate;

    @OneToMany(mappedBy = "report")
    private List<ReportService> reportServices;
}
