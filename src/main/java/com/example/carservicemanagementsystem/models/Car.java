package com.example.carservicemanagementsystem.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "cars")
@Getter
@Setter
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "car_id")
    private Integer id;
    @Column(name = "VIN")
    private String vin;
    @Column(name = "licence_plate")
    private String licencePlate;
    @Column(name = "year_of_production")
    private Integer year;
    @ManyToOne
    @JoinColumn(name = "car_model_id")
    private CarModel carModel;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "car")
    private List<Visit> visits;
}
