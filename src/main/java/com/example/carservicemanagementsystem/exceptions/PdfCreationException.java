package com.example.carservicemanagementsystem.exceptions;

public class PdfCreationException extends RuntimeException {
    public PdfCreationException(String message) {
        super(message);
    }
}
