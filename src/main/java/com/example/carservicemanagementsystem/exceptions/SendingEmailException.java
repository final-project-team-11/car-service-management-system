package com.example.carservicemanagementsystem.exceptions;

public class SendingEmailException extends RuntimeException {

    public SendingEmailException(String message) {
        super(message);
    }
}
