package com.example.carservicemanagementsystem.exceptions;

public class ExecutionDateException extends RuntimeException {

    public ExecutionDateException(String msg) {
        super(msg);
    }
}
