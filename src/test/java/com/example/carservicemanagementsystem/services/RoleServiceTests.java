package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.repositories.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.carservicemanagementsystem.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {

    @Mock
    RoleRepository mockRoleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    public void getById_Should_ThrowNotFoundException_When_InvalidId(){

        //Arrange, Act
        Mockito.when(mockRoleRepository.getById(Mockito.anyInt()))
                .thenReturn(null);
        //Assert
        Assertions.assertThrows(NotFoundException.class,
                ()->roleService.getById(Mockito.anyInt()));
    }

    @Test
    public void getById_Should_ReturnRole_When_IdIsValid(){
        Mockito.when(mockRoleRepository.getById(1))
                .thenReturn(createMockRole());

        Assertions.assertEquals(1, roleService.getById(1).getId());
    }

    @Test
    public void getByName_Should_ThrowNotFound_When_InvalidName(){

        Mockito.when(mockRoleRepository.getByName(Mockito.anyString()))
                .thenReturn(null);

        Assertions.assertThrows(NotFoundException.class,
                ()->roleService.getByName(Mockito.anyString()));
    }

    @Test
    public void getByName_Should_ReturnRole_When_NameIsValid(){
        Mockito.when(mockRoleRepository.getByName("user"))
                .thenReturn(createMockRole());

        Assertions.assertNotEquals(null, roleService.getByName("user"));
    }

    @Test
    public void getAll_Should_ReturnRoles_When_RolesAreNotEmpty(){
        Mockito.when(mockRoleRepository.findAll())
                .thenReturn(createListOfMockRoles());

        Assertions.assertEquals(3, roleService.getAll().size());
    }
}
