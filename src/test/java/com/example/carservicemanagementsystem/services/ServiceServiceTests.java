package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.Helpers;
import com.example.carservicemanagementsystem.exceptions.DuplicateEntityException;
import com.example.carservicemanagementsystem.exceptions.EntityNotFoundException;
import com.example.carservicemanagementsystem.exceptions.ForbiddenException;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.models.Service;
import com.example.carservicemanagementsystem.repositories.RoleRepository;
import com.example.carservicemanagementsystem.repositories.ServiceRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static com.example.carservicemanagementsystem.Helpers.createListOfServices;
import static com.example.carservicemanagementsystem.Helpers.createMockService;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class ServiceServiceTests {


    //    List<Service> getAll();
//
//    Service getById(Integer serviceId);
//
//    List<Service> getByServiceContain(Integer id, String serviceName, Double priceBefore, Double priceAfter);
//
//    Service create(Service service);
//
//    Service update(Integer id, Service service);
//
//    void deleteById (Integer serviceId);
    @Mock
    private ServiceRepository serviceRepository;

    @InjectMocks
    ServiceServiceImpl serviceService;

    @Test
    public void getAll_Should_ReturnListOfServices_When_ServicesExist() {
        // Arrange

        Mockito.when(serviceRepository.findAll()).thenReturn(createListOfServices());

        // Act
        List<Service> result = serviceService.getAll();

        // Assert
        Assertions.assertEquals(createListOfServices(), result);
        Mockito.verify(serviceRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void getAll_ShouldThrowNotFoundException_WhenNoServices() {
        //Arrange
        Mockito.when(serviceRepository.findAll()).thenReturn(new ArrayList<>());

        //Act & Assert
        Assertions.assertThrows(NotFoundException.class, () -> {
            serviceService.getAll();
        });
    }

    @Test
    public void getById_Should_ThrowEntityNotFoundException_When_InvalidId() {
        // Arrange
        int invalidServiceId = 999;
        Mockito.when(serviceRepository.findById(invalidServiceId)).thenReturn(Optional.empty());

        // Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> serviceService.getById(invalidServiceId)
        );
    }



}
