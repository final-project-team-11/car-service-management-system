package com.example.carservicemanagementsystem.services;

import com.example.carservicemanagementsystem.Helpers;
import com.example.carservicemanagementsystem.exceptions.NotFoundException;
import com.example.carservicemanagementsystem.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockUserRepository;
    @InjectMocks
    UserServiceImpl userService;


    @Test
    public void getById_Should_ThrowNotFoundException_When_InvalidAttribute() {
        Mockito.when(mockUserRepository.getByUserId(Mockito.anyInt()))
                .thenReturn(null);

        Assertions.assertThrows(NotFoundException.class,
                () -> userService.getById(Mockito.anyInt()));
    }

    @Test
    public void getByUsername_Should_ThrowNotFoundException_When_InvalidAttribute() {
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(null);

        Assertions.assertThrows(NotFoundException.class,
                () -> userService.getByUsername(Mockito.anyString()));
    }

    @Test
    public void getByEmail_Should_ThrowNotFoundException_When_InvalidAttribute() {
        Mockito.when(mockUserRepository.getByEmail(Mockito.anyString()))
                .thenReturn(null);

        Assertions.assertThrows(NotFoundException.class,
                () -> userService.getByEmail(Mockito.anyString()));
    }

    @Test
    public void getByPhoneNumber_Should_ThrowNotFoundException_When_InvalidAttribute() {
        Mockito.when(mockUserRepository.getByPhoneNumber(Mockito.anyString()))
                .thenReturn(null);

        Assertions.assertThrows(NotFoundException.class,
                () -> userService.getByPhoneNumber(Mockito.anyString()));
    }


    @Test
    public void getAll_Should_ReturnAll_When_ThereAreUsers(){
        Mockito.when(mockUserRepository.findAll()).thenReturn(Helpers.createListOfMockUsers());

        Assertions.assertEquals(3, userService.getAll().size());
    }
}
