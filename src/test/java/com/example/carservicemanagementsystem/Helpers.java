package com.example.carservicemanagementsystem;

import com.example.carservicemanagementsystem.models.Role;
import com.example.carservicemanagementsystem.models.Service;
import com.example.carservicemanagementsystem.models.User;

import java.util.ArrayList;
import java.util.List;

public class Helpers {

    public static Role createMockRole(){
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setRole("user");
        return mockRole;
    }

    public static List<Role> createListOfMockRoles(){
        List<Role> roles = new ArrayList<>();
        roles.add(createMockRole());
        roles.add(createMockRole());
        roles.get(1).setId(2);
        roles.add(createMockRole());
        roles.get(2).setId(3);
        return roles;
    }

    public static List<Service> createListOfServices(){
        List<Service> services = new ArrayList<>();
        Service service1 = new Service();
        service1.setServiceId(1);
        service1.setServiceName("Oil change");
        service1.setPrice(50.00);
        Service service2 = new Service();
        service2.setServiceId(2);
        service2.setServiceName("Tire rotation");
        service2.setPrice(20.00);
        services.add(service1);
        services.add(service2);
        return services;
    }

    public static User createMockUser(){
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("randomEmail@gmail.com");
        mockUser.setUsername("mockUsername");
        mockUser.setPassword("mockPassword");
        mockUser.setPhoneNumber("0870000000");
        mockUser.setRole(createMockRole());
        return mockUser;
    }

    public static List<User> createListOfMockUsers(){
        List<User> mockUsers = new ArrayList<>();
        mockUsers.add(createMockUser());
        mockUsers.add(createMockUser());
        mockUsers.add(createMockUser());
        return mockUsers;
    }

    public static Service createMockService(){
        var mockService = new Service();
        mockService.setServiceId(1);
        mockService.setServiceName("service");
        mockService.setPrice(1.20);
        return mockService;
    }
}
