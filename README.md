# Car Service Management System
A web application that enables the owners of an auto repair shop to manage their day-to-day job. 
There are three main parts (public, private and administrative).
* Public - accessible to customers without account.
* Private - It's for users that have account. And it allows them to see a list of all their services. 
* Administrative - Is only for employees/admins and it's including some sections dedicated to vehicles,
customers, visits and services.

## Functionalities
Client :
* Can add their personal cars
* Can schedule visits for their car, for a specific day and time
* Can add services to their visit.
* Can register. When it's registered the customer receives an email with automatically generated login information.
* Can see a detailed report of a personal visit.
* Can edit their own profile.

Employee :
* Can browse or update all vehicles linked to customers.
* Can create a new vehicle for customer.
* Can browse, delete, or update a customer’s profile.
* Can see all created cars and visits.
* Can see a detailed report of all visits.

# Technologies
* Spring boot
* Java
* Hibernate
* Spring Data Jpa
* Lombok
* MariaDB